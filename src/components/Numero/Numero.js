// @flow

import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import Swiper from './Swiper';
import Article from '../Article/Article';
import HeaderArticle from '../Article/HeaderArticle';
import Animated from 'react-native-reanimated';

const { Value} = Animated;

type Props = {
  navigation: any,
};

type State = any;

class Numero extends Component<Props, State> {
  static navigationOptions = {
    title: 'Articles',
  };

  constructor(props) {
    super(props);

    this.state = {
      articleIndex: props.navigation.state.params.articleIndex,
    };
    // scrollY est une value d'animated
    this.scrollY = new Value(0);
    // onArticleScroll est un objet utilisé pour mapper dans la scrollview d'article
    this.onArticleScroll = Animated.event([{nativeEvent: {contentOffset: {y: this.scrollY}}}], {
      useNativeDriver: true
    })
    this.changeIndex = this.changeIndex.bind(this);
  }
  scrollY: any

  
  // changeIndex ne doit pas être flêchée sinon elle ferait re-rendre le swiper (générée à chaque fois)
  changeIndex(index) {
    this.setState({articleIndex: index});
  };



  render() {
    const {
      navigation,
      navigation: {
        state: {
          params: {articles, articleIndex, swiperName, mapping},
        },
      },
    } = this.props;


    return (
      <View style={{flex: 1}}>
        <HeaderArticle
          swiperName={swiperName ? swiperName : ''}
          articleIndex={this.state.articleIndex}
          numeroLength={articles.length}
          mapping={mapping}
          scrollY={this.scrollY}
          navigation={navigation}
        />
        <Swiper
          originalIndex={articleIndex}
          changeIndex={this.changeIndex}
          articles={articles}
          navigation={navigation}
          onArticleScroll={this.onArticleScroll}
          swiperName={swiperName}
          numeroLength={articles.length}
          mapping={mapping}
          scrollY={this.scrollY}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  favorites: state.favorites,
  status: state.status,
});

export default connect(mapStateToProps)(Numero);
