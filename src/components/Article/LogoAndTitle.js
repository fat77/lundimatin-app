// @flow

import React from 'react';
import {View} from 'react-native';
import HTML from 'react-native-render-html';
import type {NavigationScreenProp} from 'react-navigation';

import {fonts} from '../../css/fonts';
import ImageSmart from './ImageSmart';
import {isIOS} from '../../utils/platform';

type Props = {
  article: Object,
  images: Array<any>,
  fromArticleOnly?: boolean,
  navigation: NavigationScreenProp<>,
};

const ImageAndTitle = ({
  article,
  images,
  navigation,
  fromArticleOnly,
}: Props) => (
  <View>
    <View style={{marginTop: fromArticleOnly ? 30 : 100}}>
      <ImageSmart
        navigation={navigation}
        url={article.logo_petit}
        contextImages={images}
      />
    </View>

    <HTML
      textSelectable
      html={`<h1>${article.titre}</h1>`}
      allowedStyles={[]}
      tagsStyles={{
        h1: {
          letterSpacing: 1.2,
          fontWeight: isIOS ? null : 'normal',
          fontFamily: fonts.titleBold,
          paddingTop: 30,
          textAlign: 'center',
          fontSize: 35,
          lineHeight: 32,
          marginHorizontal: 20,
        },
      }}
      alterData={node => {
        const {parent, data} = node;
        if (
          parent &&
          parent.next &&
          parent.next.data &&
          parent.next.data === ' '
        ) {
          return `${data} `;
        }
        if (parent && parent.name === 'h1') {
          return data.toUpperCase();
        }

        return false;
      }}
    />
  </View>
);

export default ImageAndTitle;
