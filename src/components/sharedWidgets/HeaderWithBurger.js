// @flow

import React from 'react';
import {StyleSheet} from 'react-native';
import {Header, Left, Right, Body, Title, Button, Icon} from 'native-base';

import type {NavigationScreenProp} from 'react-navigation';
import {colors} from '../../css/cssVariables';
import {fonts} from '../../css/fonts';

const styles = StyleSheet.create({
  header: {
    shadowColor: 'transparent',
    elevation: 1,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    alignItems: 'center',
  },
  black: {
    color: colors.blackLM,
    fontFamily: fonts.lmLogoRegular,
  },
    icon: {
    color: colors.blackLM,
  },
});

type Props = {
  title: string,
  navigation: NavigationScreenProp<{}>,
  rightFunction?: any,
};

const HeaderWithBurger = ({title, navigation, rightFunction}: Props) => (
  <Header androidStatusBarColor="#000000" transparent style={styles.header}>
    <Left>
      <Button transparent onPress={() => navigation.openDrawer()}>
        <Icon style={styles.black} name="menu" />
      </Button>
    </Left>
    <Body>
      <Title style={styles.black}>{title}</Title>
    </Body>
    <Right>
      {Boolean(rightFunction) && (
        <Button transparent onPress={rightFunction}>
          <Icon style={styles.icon} type="Entypo" name="dots-three-vertical" />
        </Button>
      )}
    </Right>
  </Header>
);

export default HeaderWithBurger;
