import {
  all,
  put,
  call,
  takeLeading,
} from 'redux-saga/effects';

import actions, { types } from './actions';

import API from '../../api/requests';


function* themesUpdater() {
  try {
    const themes = yield call(API.getThemes);
    yield put(actions.updateSuccess(themes));
  } catch (error) {
    yield put(actions.updateFailure(error));
  }
}

function* themesUpdateWatcher() {
  yield takeLeading(types.UPDATE_REQUEST, themesUpdater);
}

function* themeSaga() {
  yield all([
    themesUpdateWatcher(),
  ]);
}

export default themeSaga;
