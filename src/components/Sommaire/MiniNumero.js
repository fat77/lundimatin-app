// @flow

import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import type {NavigationScreenProp} from 'react-navigation';
import type {articleType} from '../../type';
import MiniArticle from '../MiniArticle';
import GetDate from '../sharedWidgets/GetDate';
import {fonts} from '../../css/fonts';

const styles = StyleSheet.create({
  numContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  num: {
    fontSize: 90,
    fontStyle: 'italic',
    color: 'rgb(110, 117, 115)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  date: {
    fontFamily: fonts.sansSerifItalic,
    fontStyle: 'normal',
    color: 'rgb(110, 117, 115)',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 20,
  },
});

type Props = {
  articles: Array<articleType>,
  numero: string,
  navigation: NavigationScreenProp<{
    params: {
      articleIndex: number,
      articles: Array<articleType>,
      swiperName: string,
    },
  }>,
};

const MiniNumero = ({articles, numero, navigation}: Props) => (
  <View>
    <View style={styles.numContainer}>
      <Text style={styles.num}>#{numero}</Text>
      <GetDate numDate={articles[0].date_publication} sommaireStyle />
    </View>
    {articles
      .sort((a, b) => parseInt(a.rang, 10) - parseInt(b.rang, 10))
      .map((article, index) => (
        <MiniArticle
          key={article.id}
          article={article}
          onPress={() =>
            navigation.navigate('Numero', {
              articleIndex: index,
              articles,
              swiperName: `#${numero}`,
            })
          }
        />
      ))}
  </View>
);

export default MiniNumero;
