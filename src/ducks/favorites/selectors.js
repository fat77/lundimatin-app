// @flow
import { createSelector } from 'reselect';
import type { stateType, localImageType, articleType } from '../../type';


const getFavoritesData = (state: stateType): Array<articleType> => state.favorites.data;

const getFavoriteList: stateType => Array<string> = createSelector(
  getFavoritesData,
  favorites => (favorites.length ? favorites.map(fav => fav.id) : []),
);

const getFavoritesImageList: stateType => Array<localImageType> = createSelector(
  getFavoritesData,
  favorites => favorites.reduce(((acc, cur) => (
    [
      ...acc,
      {
        name: cur.logo_petit.split('/').slice(-1).toString(),
        remoteSmall: cur.logo_petit,
        remoteBig: cur.logo,
        width: cur.logo_petit_largeur,
        height: cur.logo_petit_hauteur,
      },
      ...cur.images.map(img => ({
        name: img.url_petit.split('/').slice(-1).toString(),
        remoteSmall: img.url_petit,
        remoteBig: img.url,
        width: img.largeur,
        height: img.hauteur,
      })),
    ]
  )), []),
);

const getFavoritesImagesLocalNames: stateType => Array<string> = createSelector(
  getFavoritesImageList,
  images => images.map(image => image.name),
);

// Get the remote url of the small image
const getFavoritesImagesRemoteSmallUrls: stateType => Array<string> = createSelector(
  getFavoritesImageList,
  images => images.map(image => image.remoteSmall),
);

// Get the remote Url of the big image
const getFavoritesImagesRemoteBigUrls: stateType => Array<string> = createSelector(
  getFavoritesImageList,
  images => images.map(image => image.remoteBig),
);

export default {
  getFavoritesData,
  getFavoriteList,
  getFavoritesImageList,
  getFavoritesImagesLocalNames,
  getFavoritesImagesRemoteSmallUrls,
  getFavoritesImagesRemoteBigUrls,
};
