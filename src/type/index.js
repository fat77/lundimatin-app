// @flow

export type remoteImageType = {
  id: string,
  titre: string,
  descriptif: string,
  url: string,
  url_petit: string,
  type: string,
  date: string,
  largeur: string,
  hauteur: string,
};

export type authorType = {
  nom: string,
  bio: string,
  logo: string,
}

export type articleType = {
  id: string,
  auteurs: Array<authorType>,
  date_publication: string,
  date_modification: string,
  titre: string,
  soustitre: string,
  logo: string,
  logo_mini: string,
  logo_petit: string,
  logo_petit_hauteur: string,
  logo_petit_largeur: string,
  url_api: string,
  url: string,
  rang: string,
  couleur: string,
  temps_lecture: string,
  popularite: number,
  chapo: string,
  texte: string,
  documents: Array<string>,
  images: Array<remoteImageType>,
  themes: Array<string>,
  mots_clefs: Array<string>,
  recommandations: Array<string>,
  numero: {
    numero: string,
    date_modification: string,
    articles: Array<number>,
  },
  notes: string
};

export type numeroType = {
  numero: string,
  date_modification: string,
  articles: Array<articleType>,
};

export type numerosType = Array<numeroType>;
export type articlesType = Array<articleType>;

export type localImageType = {
  name: string,
  height: string,
  width: string,
  remoteBig: string,
  remoteSmall: string,
};

export type WordType = {
  id: string,
  titre: string,
  groupe_id: string,
  groupe_titre: string,
};

export type stateType = {
  recentNums: {
    data: numerosType,
    isFetching: boolean,
    newArticles: Array<string>,
  },
  favorites: {
    data: articlesType,
  },
  localImages: {
    data: Array<localImageType>,
    imagesToDownload: Array<localImageType>,
  },
  themes: Array<WordType>,
};
