// @flow
import {combineReducers} from 'redux';

// importing flowTypes
import type {stateType} from '../../type';

// shared actions
import shared from '../shared';

import {
  types,
  type AddFavorite,
  type RemoveFavorite,
  type ClearData,
} from './actions';

// Defining Action type
type actionType = AddFavorite | RemoveFavorite | ClearData;

// Defining Reducer type
type favoritesType = $PropertyType<stateType, 'favorites'>;
type dataType = $PropertyType<favoritesType, 'data'>;

function data(state: dataType = [], action: actionType) {
  switch (action.type) {
    case types.ADD_FAVORITE: {
      return [...state, action.payload.favorite];
    }
    case types.REMOVE_FAVORITE: {
      const favToRemove = action.payload.id;
      const newState = state.filter(fav => favToRemove !== fav.id);
      return newState;
    }
    case types.CLEAR_DATA: {
      return [];
    }
    case shared.types.INITIALIZE_STATE: {
      // used to clean state at app start
      if (!Array.isArray(state) || state.includes(null)) {
        return [];
      }
      return state;
    }
    default: {
      return state;
    }
  }
}

export default combineReducers({data});
