// @flow

import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import HTML from 'react-native-render-html';
import {colors} from '../../css/cssVariables';
import {fonts, sansRegBlack, sansRegGrey} from '../../css/fonts';
import {openLink} from '../sharedWidgets/functions';
import GetDate from '../sharedWidgets/GetDate';
import {renderNotesRef, renderLink, renderBlockquote} from './renderers';
import openUrlInApp from '../../link';
import type {NavigationScreenProp} from 'react-navigation';

const styles = StyleSheet.create({
  textNumAndDate: {
    ...sansRegBlack,
  },
  textNumAndDateGrey: {
    ...sansRegGrey,
  },
});

type Props = {
  article: Object,
  navigation: NavigationScreenProp<>,
  setIsModal: (x: boolean, y: string) => void,
  setRequiredNote: (x: string) => void,
  requiredNote?: null | string,
  setModalDownload: (text: string) => void,
};

const SoustitreAndChapo = ({
  requiredNote,
  article,
  navigation,
  setIsModal,
  setRequiredNote,
  setModalDownload,
}: Props) => {
  // hooks pour accentuer un lien quand on clique dessus
  const [isLinkTouched, setIsLinkTouched] = React.useState({
    isTouched: false,
    whichHref: '',
  });

  function open(evt, href) {
    const route = href.replace(/.*?:\/\//g, '');
    const routeName = route.split('/')[0];
    if (routeName === 'lundi.am/') {
      openUrlInApp({url: href}, navigation);
    } else {
      openLink(evt, href);
    }
  }
  return (
    <View>
      <View
        style={{
          paddingTop: 20,
          marginHorizontal: 20,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {article.soustitre ? (
          <HTML
            textSelectable
            html={article.soustitre}
            onLinkPress={(evt, href) => {
              open(evt, href);
            }}
            allowedStyles={[]}
            tagsStyles={{
              p: {
                fontFamily: 'OpenSans-Light',
                fontSize: 20,
                color: colors.greyLM,
                textAlign: 'center',
              },
            }}
          />
        ) : null}
      </View>
      <View
        style={{
          marginHorizontal: 10,
          paddingTop: 10,
          paddingBottom: 25,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={styles.textNumAndDate}>paru dans </Text>
        <Text style={styles.textNumAndDateGrey}>
          lundimatin #{article.numero.numero}
        </Text>

        <Text style={styles.textNumAndDate}>, le </Text>
        <GetDate sommaireStyle={false} numDate={article.date_publication} />
      </View>

      <View style={{margin: 10}}>
        {article.chapo && article.chapo.length > 0 ? (
          <HTML
            textSelectable
            html={article.chapo}
            onLinkPress={(evt, href) => {
              open(evt, href);
              openLink(evt, href);
            }}
            allowedStyles={[]}
            style={{margin: 10}}
            tagsStyles={{
              p: {
                fontWeight: 'normal',
                fontFamily: fonts.textBold,
                fontSize: 18,
                lineHeight: 25,
              },
              i: {
                fontWeight: 'normal',
                fontStyle: 'normal',
                fontFamily: fonts.textBoldItalic,
                fontSize: 18,
              },
              a: {
                fontWeight: 'normal',
                fontFamily: fonts.textBold,
                fontSize: 18,
                textDecorationLine: 'underline',
                color: 'black',
              },
            }}
            alterNode={node => {
              const {name, parent, attribs} = node; // Pour les notes on crée un node.name note qui va nous permettre de faire un renderer
              if (
                name === 'span' &&
                attribs.class &&
                attribs.class === 'spip_note_ref'
              ) {
                // le premier enfant[0] est "[", le deuxième est le "a",
                // et on veut le (seul) noeud qu'il contient, et particulièrement sa data.
                const key = node.children[1].children[0].data;
                node.name = 'note';

                node.attribs = {...node.attribs, key};
              }
              // Pour les notes on retire le tag 'a' et la href sinon on ne peut pas cliquer
              if (name === 'a' && parent.name === 'note') {
                node.name = 'p';
                node.attribs = {class: 'notes_text'};
              }
            }}
            renderers={{
              note: {
                renderer: renderNotesRef,
                renderersProps: {
                  setIsModal,
                  setRequiredNote,
                  requiredNote,
                  setIsLinkTouched,
                  isLinkTouched,
                },
                wrapper: 'Text',
              },
              a: {
                renderer: renderLink,
                renderersProps: {
                  isLinkTouched,
                  setIsLinkTouched,
                  setModalDownload,
                  setIsModal,
                },
              },
              blockquote: renderBlockquote,
            }}
          />
        ) : null}
      </View>
    </View>
  );
};

export default SoustitreAndChapo;
