// @flow

import React from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';
import Share from 'react-native-share';
import {Icon} from 'native-base';
import {connect} from 'react-redux';
import Animated from 'react-native-reanimated';
import moment from 'moment';
import 'moment/locale/fr';
// CSS
import {sizes, colors, fontsSizes} from '../../css/cssVariables';
import {fonts} from '../../css/fonts';
// DUCKS
import {favoritesSelectors, favoritesOperations} from '../../ducks/favorites';
import {recentNumsSelectors} from '../../ducks/recentNums';

const styles = StyleSheet.create({
  navBarContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: sizes.fullWidth,
    marginBottom: 5,
  },
  navBarTitle: {
    fontFamily: fonts.lmLogoRegular,
    fontStyle: 'normal',
    fontWeight: 'normal',
    color: colors.blackLM,
    fontSize: 18,
  },

  navBarIcon: {
    color: colors.goldLM,
    fontSize: 30,
    marginTop: 3,
    marginHorizontal: 16,
  },
  scrollViewContent: {
    backgroundColor: colors.whiteLM,
    paddingTop: sizes.navBarHeight,
  },
  dot: {
    backgroundColor: colors.greyLM,
    flexGrow: 1,
    height: 2,
    marginTop: 3,
    opacity: 0.2,
  },
  activeDot: {
    backgroundColor: colors.goldLM,
    flexGrow: 1,
    height: 5,
  },
  withoutDot: {
    height: 6,
  },
  date: {
    color: colors.greyLM,
    fontFamily: fonts.lmLogoRegular,
    // marginLeft: -10,
  },
});

type Props = {
  navigation: any,
  animatedTitleOpacity?: any,
  swiperName?: string,
  articleIndex?: number,
  mapping?: Array<any>,
};

// HEADERCONTENT doit pouvoir être rendu en version maximale
// (avec le mapping, l'opacité gradiente etc)
// ou minimale pour ArticleOnly

function HeaderContent({
  navigation,
  animatedTitleOpacity,
  swiperName,
  articleIndex,
  mapping,
}: Props) {
  // Bizarrement articleIndex n'est pas toujours là dès les premières secondes
  // Et ça peut empêcher la date de rendre si on le demande
  // (de toute façon il arrivera toujours en même temps que mapping)
  const date = mapping
    ? moment(mapping[articleIndex].date).format('D MMMM')
    : null;
  const titleOpacity = animatedTitleOpacity || 1;
  const title = swiperName || ' ';

  return (
    <>
      <View style={styles.navBarContainer}>
        <View>
          <TouchableOpacity onPress={() => navigation.popToTop()}>
            <Icon type="Feather" style={styles.navBarIcon} name="arrow-left" />
          </TouchableOpacity>
        </View>
        <View>
          {Boolean(swiperName) && (
            <Text style={styles.navBarTitle}>
              lundi<Text style={{fontFamily: fonts.lmLogoBold}}>matin</Text>
              {` ${title}`}
            </Text>
          )}
          <Text style={styles.date}>{date}</Text>
        </View>
      </View>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignContent: 'stretch',
          // marginTop: 10,
        }}>
        {Boolean(mapping) ? (
          mapping.map((item, index) => {
            const key = index;
            if (index === articleIndex) {
              return <View key={key} style={styles.activeDot} />;
            }
            return <View key={key} style={styles.dot} />;
          })
        ) : (
          <View style={styles.dot} />
        )}
      </View>
    </>
  );
}

export default HeaderContent;
