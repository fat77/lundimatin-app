import reducer from './reducers';

export { default as favoritesSelectors } from './selectors';
export { default as favoritesOperations } from './operations';
export { types as favoritesTypes } from './actions';

export default reducer;
