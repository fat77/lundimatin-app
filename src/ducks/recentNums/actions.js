// @flow
import type { numerosType } from '../../type';

// Types

const UPDATE_REQUEST = 'lm/recentNums/UPDATE_REQUEST';
export type UpdateRequestType = 'lm/recentNums/UPDATE_REQUEST';

const UPDATE_FAILURE = 'lm/recentNums/UPDATE_FAILURE';
export type UpdateFailureType = 'lm/recentNums/UPDATE_FAILURE';

const UPDATE_SUCCESS = 'lm/recentNums/UPDATE_SUCCESS';
export type UpdateSuccessType = 'lm/recentNums/UPDATE_SUCCESS';

const REFRESH_NEW_ARTICLES = 'lm/recentNums/REFRESH_NEW_ARTICLES';
export type RefreshNewArticlesType = 'lm/recentNums/REFRESH_NEW_ARTICLES';

const CLEAR_NEW_ARTICLES = 'lm/recentNums/CLEAR_NEW_ARTICLES';
export type ClearNewArticlesType = 'lm/recentNums/CLEAR_NEW_ARTICLES';

const REMOVE_FROM_NEW_ARTICLES = 'lm/recentNums/REMOVE_FROM_NEW_ARTICLES';
export type RemoveFromNewArticlesType = 'lm/recentNums/REMOVE_FROM_NEW_ARTICLES';

const CLEAR_DATA = 'lm/recentNums/CLEAR_DATA';
export type ClearDataType = 'lm/recentNums/CLEAR_DATA';


export type UpdateRequest = {type: UpdateRequestType}
const updateRequest = (): UpdateRequest => ({
  type: UPDATE_REQUEST,
});

export type UpdateSuccess = {type: UpdateSuccessType, payload: {nums: numerosType}}
const updateSuccess = (nums: numerosType): UpdateSuccess => ({
  type: UPDATE_SUCCESS,
  payload: { nums },
});

export type UpdateFailure = {type: UpdateFailureType, payload: {error: any}}
const updateFailure = (error: any): UpdateFailure => ({
  type: UPDATE_FAILURE,
  payload: { error },
});

export type RefreshNewArticles = {
  type: RefreshNewArticlesType, payload: {articleIds: Array<string>}
}
const refreshNewArticles = (articleIds: Array<string>): RefreshNewArticles => ({
  type: REFRESH_NEW_ARTICLES,
  payload: { articleIds },
});

export type ClearNewArticles = {
  type: ClearNewArticlesType, payload: {articleIds: Array<string>}
}
const clearNewArticles = (articleIds: Array<string>): ClearNewArticles => ({
  type: CLEAR_NEW_ARTICLES,
  payload: { articleIds },
});

export type RemoveFromNewArticles = {
  type: RemoveFromNewArticlesType, payload: {id: string}
}
const removeFromNewArticles = (id: string): RemoveFromNewArticles => ({
  type: REMOVE_FROM_NEW_ARTICLES,
  payload: { id },
});

export type ClearData = {type: ClearDataType}
const clearData = () => ({
  type: CLEAR_DATA,
});

export default {
  updateRequest,
  updateFailure,
  updateSuccess,
  refreshNewArticles,
  clearNewArticles,
  removeFromNewArticles,
  clearData,
};

export const types = {
  UPDATE_REQUEST,
  UPDATE_FAILURE,
  UPDATE_SUCCESS,
  REFRESH_NEW_ARTICLES,
  REMOVE_FROM_NEW_ARTICLES,
  CLEAR_NEW_ARTICLES,
  CLEAR_DATA,
};
