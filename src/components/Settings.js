// @flow

import React from 'react';
import { Container, Text } from 'native-base';
import { StyleSheet, View } from 'react-native';
import {  colors } from '../css/cssVariables';

const styles = StyleSheet.create({
  auMilieu: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  Bientot: {
    fontSize: 25,
    color:colors.greyLM,
  },
});

const Settings = () => (
  <Container>
    <View style={styles.auMilieu}>
      <Text style={styles.Bientot}>Bientôt dans l'app</Text>
    </View>
  </Container>
);

export default Settings;
