// @flow
import { createSelector } from 'reselect';

import type { stateType, localImageType } from '../../type';

const getLocalImages = (state: stateType): Array<localImageType> => state
  .localImages.data;
const getImagesToDownload = (state: stateType): Array<localImageType> => state
  .localImages.imagesToDownload;


const getLocalImagesNames: stateType => Array<string> = createSelector(
  getLocalImages,
  images => images.map(image => image.name),
);


export default { getLocalImages, getLocalImagesNames, getImagesToDownload };
