import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
// import { goToTop } from './functions';
import { scrollUp } from '../../css/cssVariables';

const goToTop = (scroll) => {
  scroll.scrollTo({ x: 0, y: 0, animated: true });
};

const ScrollUp = ({ scroll }) => (
  <TouchableOpacity
    activeOpacity={1}
    style={scrollUp.button}
    onPress={() => goToTop(scroll)}
  >
    <Icon style={scrollUp.icon} type="AntDesign" name="upcircleo" />
  </TouchableOpacity>
);

export default ScrollUp;
