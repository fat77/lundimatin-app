import {all} from 'redux-saga/effects';

import {recentNumsSaga} from './ducks/recentNums';
// import {localImagesSaga} from './ducks/localImages';
import {themesSaga} from './ducks/themes';

// SAGAS
export default function* rootSaga() {
  yield all([
    recentNumsSaga(),
    // localImagesSaga(),
    themesSaga(),
  ]);
}
