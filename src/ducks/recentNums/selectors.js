// @flow
import { createSelector } from 'reselect';

import type { stateType, localImageType, numerosType } from '../../type';

type recentNumsType = $PropertyType<stateType, 'recentNums'>
type dataType = $PropertyType<recentNumsType, 'data'>
type isFetchingType = $PropertyType<recentNumsType, 'isFetching'>
type newArticlesType = $PropertyType<recentNumsType, 'newArticles'>

const getRecentNumsData = (state: stateType): dataType => state.recentNums.data;
const getFetchingStatus = (state: stateType): isFetchingType => state.recentNums.isFetching;
const getNewArticles = (state: stateType): newArticlesType => state.recentNums.newArticles;

// Get a sorted array of nums and articles in it.
const getSortedNums: stateType => numerosType = createSelector(
  getRecentNumsData,
  nums => nums.sort((a, b) => parseInt(b.numero, 10) - parseInt(a.numero, 10))
    .map(numero => ({
      date_modification: numero.date_modification,
      numero: numero.numero,
      articles: numero.articles.sort((a, b) => parseInt(a.rang, 10) - parseInt(b.rang, 10)),
    })),
);


// Get an array of recent nums ids
const getRecentNumList: stateType => Array<string> = createSelector(
  getRecentNumsData,
  nums => nums.map(num => num.numero),
);

// Get an array of recent articles ids
const getRecentArticleList: stateType => Array<string> = createSelector(
  getRecentNumsData,
  nums => nums.reduce(((acc, cur) => (
    [...acc, ...cur.articles.map(article => article.id)]
  )), []),
);


// Get an array of objects : local and remote urls
const getRecentImageList: stateType => Array<localImageType> = createSelector(
  getRecentNumsData,
  nums => nums.reduce(((accNumsImages, curNum) => (
    [
      ...accNumsImages,
      ...curNum.articles.reduce(((accArticlesImages, curArticle) => (
        [
          ...accArticlesImages,
          {
            // On enregistre le petit en local
            name: curArticle.logo_petit.split('/').slice(-1).toString(),
            // Et on garde trace du grand en ligne
            remoteSmall: curArticle.logo_petit,
            remoteBig: curArticle.logo,
            width: curArticle.logo_petit_largeur,
            height: curArticle.logo_petit_hauteur,
          },
          ...curArticle.images.map(img => ({
            name: img.url_petit.split('/').slice(-1).toString(),
            remoteSmall: img.url_petit,
            remoteBig: img.url,
            width: img.largeur,
            height: img.hauteur,
          })),
        ]
      )), []),
    ]
  )), []),
);

// Get the name of a recent image
const getRecentImagesLocalNames: stateType => Array<string> = createSelector(
  getRecentImageList,
  images => images.map(image => image.name),
);

// Get the remote url of the small image
const getRecentImagesRemoteSmallUrls: stateType => Array<string> = createSelector(
  getRecentImageList,
  images => images.map(image => image.remoteSmall),
);

// Get the remote Url of the big image
const getRecentImagesRemoteBigUrls: stateType => Array<string> = createSelector(
  getRecentImageList,
  images => images.map(image => image.remoteBig),
);

export default {
  getRecentNumsData,
  getSortedNums,
  getRecentNumList,
  getNewArticles,
  getFetchingStatus,
  getRecentArticleList,
  getRecentImageList,
  getRecentImagesLocalNames,
  getRecentImagesRemoteSmallUrls,
  getRecentImagesRemoteBigUrls,
};
