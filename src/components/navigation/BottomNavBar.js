// @flow

import React from 'react';
import {Button, Text, Icon, Footer, FooterTab, View} from 'native-base';
import type {NavigationScreenProp, NavigationState} from 'react-navigation';
import {colors, sizes} from '../../css/cssVariables';

type Props = {
  navigationState: NavigationState,
  navigation: NavigationScreenProp<{}>,
};

const colorPicker = (indexToMatch, navigationState) => {
if (navigationState.index === indexToMatch) return colors.goldLM
else return colors.blackLM
}

const BottomNavBar = ({navigationState, navigation}: Props) => (
  <Footer>
    <FooterTab >
      <View style={{ position: 'absolute', top:0, height: 2, width: sizes.fullWidth, backgroundColor: colors.greyLM, opacity: 0.2}} />
      <Button
        vertical
        // active={navigationState.index === 0}
        onPress={() => navigation.navigate('Sommaire')}>
        <Icon name="home" style={{fontSize: 30, color: colorPicker(0, navigationState)}} />
      </Button>
      <Button
        vertical
        // active={navigationState.index === 1}
        onPress={() => navigation.navigate('Favoris')}>
        <Icon type="MaterialIcons" style={{fontSize: 30, color: colorPicker(1, navigationState)}} name="bookmark-border" />
      </Button>
      <Button
        vertical
        // active={navigationState.index === 2}
        onPress={() => navigation.navigate('Rechercher')}>
        <Icon type="MaterialIcons" style={{fontSize: 30, color: colorPicker(2, navigationState)}} name="search" />
      </Button>
    </FooterTab>
  </Footer>
);

export default BottomNavBar;
