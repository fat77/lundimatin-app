import React, {Component, useState, useRef} from 'react';
import {
  View,
  ImageBackground,
  TouchableOpacity,
  Text,
  Linking,
  Image,
  PermissionsAndroid,
} from 'react-native';
import {Icon} from 'native-base';
import AutoHeightWebView from 'react-native-autoheight-webview';
import RNFetchBlob from 'rn-fetch-blob';

import * as WebBrowser from 'expo-web-browser';
import {isIOS} from '../../utils/platform';

import {sizes, fontsSizes, colors} from '../../css/cssVariables';
import {fonts} from '../../css/fonts';
import {isAndroid} from '../../utils/platform';
import ImageSmart from './ImageSmart';

import {WebView} from 'react-native-webview';
import Video from 'react-native-video';

import Slider from '@react-native-community/slider';

export function renderPdfDocument() {
  return (
    <Text
      style={{
        fontWeight: 'normal',
        fontFamily: fonts.textLightItalic,
        fontSize: fontsSizes.navBar,
        textDecorationLine: 'underline',
        color: colors.blackLM,
      }}>
      <Icon
        style={{
          fontSize: fontsSizes.navBar,
          color: colors.blackLM,
        }}
        type="AntDesign"
        name="pdffile1"
      />{' '}
      télécharger le document
    </Text>
  );
}

export function renderh2Link(
  htmlAttribs,
  children,
  convertedCSSStyles,
  passProps,
) {
  const {
    isLinkTouched,
    setIsLinkTouched,
  } = passProps.renderers.a.renderersProps;
  const {href} = htmlAttribs;
  return (
    <Text
      onPress={() => {
        setIsLinkTouched({isTouched: true, whichHref: href});
        Linking.openURL(href);
        setTimeout(() => {
          setIsLinkTouched({isTouched: false, whichHref: ''});
        }, 250);
      }}
      style={{
        backgroundColor:
          isLinkTouched.isTouched && href === isLinkTouched.whichHref
            ? colors.goldLM
            : null,
      }}>
      {children}
    </Text>
  );
}

export function renderLink(
  htmlAttribs,
  children,
  convertedCSSStyles,
  passProps,
) {
  const {
    isLinkTouched,
    setIsLinkTouched,
    setIsModal,
    setModalDownload,
  } = passProps.renderers.a.renderersProps;
  const {href} = htmlAttribs;
  if (href === undefined) {
    return;
  }
  // SI C'EST UN PDF À TÉLÉCHARGER

  // on regarde si l'url finit par .pdf
  const isPdf = href.substr(href.length - 3);
  // on check si l'url vient d'internet et pas de l'api
  const isExternalLink = href.substr(0, 4);

  // si c'est le cas on le télécharge
  if (isPdf === 'pdf' && isExternalLink !== 'http') {
    // découpage pour avoir le nom du pdf
    const pathArray = href.split('/');
    const pdfName = pathArray.pop();
    // les dossiers du téléphone
    const dirs = isAndroid
      ? RNFetchBlob.fs.dirs.DownloadDir
      : RNFetchBlob.fs.dirs.DocumentDir;
    // la fonction qui télécharge
    const downloadFile = () => {
      let pdfLink = `https://lundi.am${href}`;
      if (href[0] !== '/') {
        pdfLink = `https://lundi.am/${href}`;
      }
      RNFetchBlob.config({
        // add this option that makes response data to be stored as a file,
        // this is much more performant.
        path: dirs + `/${pdfName}`,
        fileCache: true,
      })
        .fetch('GET', pdfLink, {
          //some headers ..
        })
        .then(res => {
          // the temp file path
          // pour ouvrir la modale
          setIsModal(true, 'download modal');
          // le texte de la modale
          if (isAndroid) {
            setModalDownload(
              `le fichier: "${pdfName}" a été téléchargé dans le dossier: "Download"`,
            );
          } else {
            `le fichier: "${pdfName}" a été téléchargé`;
          }

          console.log('The file is saved to ', res.path());
        })
        .catch(err => {
          console.log(err);
        });
    };

    // demande de permissions sur android
    const requestFSPermission = async () => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Permissions pour enregistrer des fichiers',
            message:
              "lundimatin a besoin d'une permission pour enregistrer des fichiers sur la mémoire de votre téléphone,",
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can download the file');
          downloadFile();
        } else {
          console.log('file permission denied');
        }
      } catch (err) {
        console.warn(err);
      }
    };
    return (
      <Text
        onPress={() => {
          setIsLinkTouched({isTouched: true, whichHref: href});
          isAndroid ? requestFSPermission() : downloadFile();
          setTimeout(() => {
            setIsLinkTouched({isTouched: false, whichHref: ''});
          }, 250);
        }}
        style={
          isLinkTouched.isTouched &&
          href === isLinkTouched.whichHref && {
            backgroundColor: colors.goldLM,
            //color: 'blue',
          }
        }>
        {children}
      </Text>
    );
  }

  if (children[0] && children[0][0] && children[0][0].type === 'RCTView') {
    return;
  } else {
    return (
      <Text
        onPress={() => {
          setIsLinkTouched({isTouched: true, whichHref: href});
          Linking.openURL(href);
          setTimeout(() => {
            setIsLinkTouched({isTouched: false, whichHref: ''});
          }, 250);
        }}
        style={
          isLinkTouched.isTouched &&
          href === isLinkTouched.whichHref && {
            backgroundColor: colors.goldLM,
            //color: 'blue',
          }
        }>
        {children}
      </Text>
    );
  }
}

export function renderNotesRef(
  htmlAttribs,
  children,
  convertedCSSStyles,
  passProps,
) {
  const {
    setIsModal,
    setRequiredNote,
    requiredNote,
    setIsLinkTouched,
    isLinkTouched,
  } = passProps.renderers.note.renderersProps;
  const {key} = htmlAttribs; // l'id de la note (passé dans l'alterNode)

  return (
    <Text
      onPress={() => {
        setIsModal(true, 'note modal');
        setRequiredNote(key);
        setIsLinkTouched({isTouched: true});
        setTimeout(() => {
          setIsLinkTouched({isTouched: false, whichHref: ''});
        }, 250);
      }}
      style={
        isLinkTouched.isTouched &&
        key === requiredNote && {
          backgroundColor: colors.goldLM,
        }
      }>
      {children}
    </Text>
  );
}

export function renderNameAndBio(
  htmlAttribs,
  children,
  convertedCSSStyles,
  passProps,
) {
  return (
    <View style={{marginBottom: 20, margin: 5}}>
      <Text style={{textAlign: 'center'}}>
        <Text
          style={{
            color: colors.redLM,
            fontFamily: fonts.textRegular,
            fontSize: fontsSizes.bodyArticle - 1,
          }}>
          {children[0]}
        </Text>
        <Text
          style={{
            color: colors.blackLM,
            fontFamily: fonts.textRegular,
            fontSize: fontsSizes.bodyArticle - 1,
          }}>
          {children[1]}
        </Text>
      </Text>
    </View>
  );
}

export function renderBlockquote(
  htmlAttribs,
  children,
  convertedCSSStyles,
  passProps,
) {
  // twitter-widget perso
  if (htmlAttribs.class === 'twitter-tweet') {
    const twitterWidget =
      `<div style="padding: 10px 0; width: 90%; margin: auto">${
        htmlAttribs.innerHTML
      }</div>` +
      '<script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>';

    let tweetRef;
    return (
      <AutoHeightWebView
        ref={ref => {
          tweetRef = ref;
        }}
        onNavigationStateChange={navigator => {
          if (navigator.url !== 'about:blank') {
            tweetRef.stopLoading(); //GROS HACK DE MALADE
            if (isIOS) {
              Linking.canOpenURL(navigator.url).then(supported => {
                if (supported) {
                  Linking.openURL(navigator.url);
                }
              });
            } else {
              WebBrowser.openBrowserAsync(navigator.url);
            }
          }
        }}
        style={{width: sizes.fullWidth - 20}}
        source={{html: twitterWidget}}
      />
    );
  }
  return (
    <View style={{marginTop: 50, margin: 30}}>
      <Image
        source={require('../../../assets/frenchLeftQuotationMark.png')}
        style={{
          height: 100,
          width: 100,
          position: 'absolute',
          top: -35,
          left: -35,
        }}
      />
      {children}
    </View>
  );
}

export function renderAudio(htmlAttribs) {
  const {url} = htmlAttribs;
  return <AudioPlayer url={url} />;
}

function AudioPlayer(props) {
  const [onPause, setOnPause] = useState(true);
  const [audioDuration, setAudioDuration] = useState(0); //En secondes
  const [sliderPosition, setSliderPosition] = useState(0);
  const refPlayer = useRef(null); //Pour lier player et slider
  const {url} = props;

  const secondsToTime = sec => {
    let time = '';

    sec = Math.floor(sec);
    const minute = Math.floor(sec / 60);
    const seconde = Math.floor(sec % 60);

    if (minute < 10) {
      time += '0';
    }
    time += minute;

    time += ':';

    if (seconde < 10) {
      time += '0';
    }
    time += seconde;

    return time;
  };

  return (
    <View
      style={{
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
      }}>
      <TouchableOpacity
        onPress={() => {
          onPause === false ? setOnPause(true) : setOnPause(false);
        }}>
        {onPause === false ? (
          <Icon type="AntDesign" name="pause" />
        ) : (
          <Icon type="AntDesign" name="caretright" />
        )}

        <Video
          onLoad={e => setAudioDuration(e.duration)}
          onProgress={e => {
            setSliderPosition(e.currentTime);
          }}
          source={{uri: url}}
          paused={onPause}
          // poster="https://lundi.am/IMG/arton2457-resp2800.jpg"
          audioOnly
          ref={refPlayer}
        />
      </TouchableOpacity>
      <Slider
        value={sliderPosition}
        onValueChange={currentPosition => setSliderPosition(currentPosition)}
        onSlidingComplete={() => {
          //Quand on relâche le bouton du slider
          refPlayer.current.seek(sliderPosition); //On dit au player de se positionner au bon moment
        }}
        style={{flex: 1}}
        minimumValue={0}
        maximumValue={audioDuration}
        minimumTrackTintColor="#000000"
        maximumTrackTintColor="#000000"
        thumbTintColor="#000000"
      />
      <Text>
        {secondsToTime(sliderPosition)}/{secondsToTime(audioDuration)}
      </Text>
    </View>
  );
}

export function renderIframe(htmlAttribs) {
  const {height, width, src} = htmlAttribs;

  if (htmlAttribs.class === 'scribd_iframe_embed') {
    return null;
  }

  const ratio =
    height && height !== undefined
      ? parseInt(height, 10) / parseInt(width, 10)
      : 0.5625;
  const newWidth =
    width && width !== undefined
      ? Math.floor(sizes.fullWidth < width ? sizes.fullWidth : width) - 20
      : sizes.fullWidth - 20;
  const newHeight = Math.floor(newWidth * ratio);
  const srcArray = src.split('/');
  const newSrc = srcArray[4].split('?');
  const host = srcArray[2];

  switch (host) {
    case 'www.youtube.com':
    case 'www.youtube-nocookie.com': {
      return (
        <YouTubeVid
          src={src}
          key={newSrc[0]}
          id={newSrc[0]}
          width={newWidth}
          height={newHeight}
        />
      );
    }
    case 'player.vimeo.com': {
      return (
        <VimeoVid
          src={src}
          key={newSrc[0]}
          id={newSrc[0]}
          width={newWidth}
          height={newHeight}
        />
      );
    }

    default: {
      /* Si ce n'est ni Youtube ou Vimeo.
      On affiche une Webview (qui est une petite fenetre ouverte sur le web)

      */
      let videoPlayer;
      return (
        <View style={{marginVertical: 20}}>
          <WebView
            ref={ref => {
              videoPlayer = ref;
            }}
            onNavigationStateChange={navigator => {
              /* Pour éviter que l'on circule dans le Web à l'intérieur de la Webview,
              Si l'url change (n'est plus "about:blank"), on empeche le chargement */
              if (navigator.url !== 'about:blank') {
                videoPlayer.stopLoading(); //GROS HACK DE MALADE
              }
            }}
            style={{width: '100%', height: 200}}
            source={{
              html:
                '<html><meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" /><iframe src=' +
                src +
                ' frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height:100%;width:100%;position:absolute;top:0px;left:0px;right:0px;bottom:0px" height="100%" width="100%"></iframe></html>',
            }}
          />
        </View>
      );
    }
  }
}

function YouTubeVid({src, id, width, height}) {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={{marginVertical: 10}}
      onPress={() => {
        if (isIOS) {
          Linking.canOpenURL(src).then(supported => {
            if (supported) {
              Linking.openURL(src);
            }
          });
        } else {
          WebBrowser.openBrowserAsync(src);
        }
      }}>
      <ImageBackground
        style={{
          width,
          height,
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: colors.blackLM,
        }}
        source={{
          uri: `https://img.youtube.com/vi/${id}/sddefault.jpg`,
        }}>
        <Icon
          style={{
            color: 'white',
            fontSize: height,
            margin: 10,
          }}
          type="EvilIcons"
          name="play"
        />
      </ImageBackground>
    </TouchableOpacity>
  );
}

class VimeoVid extends Component {
  state = {thumbnail: null};

  componentDidMount() {
    const {src, id, width, height} = this.props;

    fetch(`http://vimeo.com/api/v2/video/${id}.json`)
      .then(response => response.json())
      .then(parsed => this.setState({thumbnail: parsed[0].thumbnail_medium}));
  }

  render() {
    const {src, id, width, height} = this.props;
    const {thumbnail} = this.state;
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        key={id}
        style={{marginVertical: 10}}
        onPress={() => {
          if (isIOS) {
            Linking.canOpenURL(src).then(supported => {
              if (supported) {
                Linking.openURL(src);
              }
            });
          } else {
            WebBrowser.openBrowserAsync(src);
          }
        }}>
        <ImageBackground
          style={{
            width,
            height,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: colors.blackLM,
          }}
          source={{
            uri: thumbnail,
          }}>
          <Icon
            style={{
              color: 'white',
              fontSize: height,
              margin: 10,
            }}
            type="EvilIcons"
            name="play"
          />
        </ImageBackground>
      </TouchableOpacity>
    );
  }
}

export function renderImage(
  htmlAttribs,
  children,
  convertedCSSStyles,
  passProps,
) {
  const {src} = htmlAttribs;
  const className = htmlAttribs.class;
  const {navigation, images} = passProps.renderers.img.renderersProps;
  if (className === 'puce') return false;
  if (!src) return false;

  // Logique pour récupérer une image différente (plus grande ou plsu petite...)
  // const radical = src.split("/")[3].split("-")[0]
  // const smallImage = images.map(i => i.remoteSmall).find(i => i.includes(radical))
  // console.log(sizes.fullWidth);
  return (
    <View key={src} style={{marginTop: 10, marginBottom: 10}}>
      <ImageSmart url={src} navigation={navigation} contextImages={images} />
    </View>
  );
}

export function renderBuzzsprout(
  htmlAttribs,
  children,
  convertedCSSStyles,
  passProps,
) {
  // On retrouve le nom de la div qui doit servir à accueillir le script (qui est en sibling et non en parent)
  const containerId = htmlAttribs.src;
  let player;
  return (
    <View style={{marginVertical: 20}}>
      <WebView
        ref={ref => {
          player = ref;
        }}
        onNavigationStateChange={navigator => {
          /* Pour éviter que l'on circule dans le Web à l'intérieur de la Webview,
              Si l'url change (n'est plus "about:blank"), on empeche le chargement */
          if (navigator.url !== 'about:blank') {
            player.stopLoading(); //GROS HACK DE MALADE
          }
        }}
        style={{width: '100%', height: 100}}
        source={{
          html: `<div id="${containerId}"></div> \n<script src="${
            htmlAttribs.src
          }" type="text/javascript" charset="utf-8"></script>`,
        }}
      />
    </View>
  );
}
