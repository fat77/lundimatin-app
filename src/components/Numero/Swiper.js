// @flow

import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import RNSwiper from 'react-native-swiper';
import Article from '../Article/Article';
import HeaderArticle from '../Article/HeaderArticle';

type Props = {
  originalIndex: number,
  changeIndex: () => void,
  articles: Array<any>,
  navigation: any,
  onArticleScroll: () => void,
  swiperName: string,
  numeroLength: number,
  scrollY: any,
  mapping: any[],
};

const Swiper = ({
  originalIndex,
  changeIndex,
  articles,
  navigation,
  onArticleScroll,
  swiperName,
  numeroLength,
  mapping,
  scrollY,
}: Props) => {
  return (
    <RNSwiper
      loop={false}
      loadMinimal
      index={originalIndex}
      onIndexChanged={changeIndex}
      showsPagination={false}>
      {articles.map((article, index) => (
        <Article
          key={article.id}
          indexNum={index}
          article={article}
          navigation={navigation}
          articles={articles}
          onArticleScroll={onArticleScroll}
          swiperName={swiperName}
          numeroLength={numeroLength}
          mapping={mapping}
          scrollY={scrollY}
        />
      ))}
    </RNSwiper>
  );
};

// Swiper.whyDidYouRender = true;

// React.memo Evite les re-rendus sans changements de props causés par Numero.
export default React.memo<Props>(Swiper);
