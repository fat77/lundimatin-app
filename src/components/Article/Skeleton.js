// @flow
import React from 'react';
import {Rect} from 'react-native-svg';
// import SvgAnimatedLinearGradient from 'react-native-svg-animated-linear-gradient';
import ContentLoader from 'rn-content-loader';
import {sizes} from '../../css/cssVariables';

type Props = {
  numberFromProps: number,
};

const Skeleton = ({numberFromProps}: Props) => {
  const height = numberFromProps * 20;
  const svgs = [];
  (function generateSvgs(number) {
    let i = 0;
    while (i < number) {
      svgs.push({
        x: '0',
        y: (i * 20).toString(),
        rx: '3',
        ry: '4',
        width: (
          (sizes.fullWidth - 20) - Math.floor(Math.random() * Math.floor(4)) * 7
        ).toString(),
        height: '10',
      });
      i += 1;
    }
  })(numberFromProps);

  return (
    <ContentLoader
      primaryColor="#bebebe"
      secondaryColor="#606060"
      width={sizes.fullWidth -10}
      height={height}>
      
      {svgs.map(svg => (
        // <Svg>
        <Rect
          key={svg.y}
          x={svg.x}
          y={svg.y}
          rx={svg.rx}
          ry={svg.ry}
          width={svg.width}
          height={svg.height}
        />
        // </Svg>
      ))}
    </ContentLoader>
  );
};

export default Skeleton;
