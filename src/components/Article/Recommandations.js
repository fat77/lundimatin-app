import React from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';

import type {NavigationScreenProp} from 'react-navigation';
import MicroArticle from '../MicroArticle';
import API from '../../api/requests';

import type {articleType} from '../../type';
import {recentNumsSelectors} from '../../ducks/recentNums';

type Props = {
  article: articleType,
  recentNums: Array<mixed>,
  recentNumList: Array<string>,
  navigation: NavigationScreenProp<{
    params: {
      articleIndex: number,
      articles: Array<articleType>,
      swiperName: string,
    },
  }>,
};

const Recommandations = ({
  article,
  recentNums,
  recentNumList,
  navigation,
}: Props) => (
  <View>
    {article.recommandations.map(reco => (
      <MicroArticle
        key={reco.id}
        article={reco}
        onPress={async () => {
          // A priori on ramène vers le ArticleOnly le plus proche dans l'arbre
          // de navigation, et on le remplace à chaque fois  par le suivant...
          const article = await API.getArticle(reco.id);
          navigation.push('ArticleOnly', {
            indexNum: 0,
            article,
            swiperName: `#${reco.numero.numero}`,
          });
        }}
      />
    ))}
  </View>
);

const mapStateToProps = state => ({
  recentNums: recentNumsSelectors.getRecentNumsData(state),
  recentNumList: recentNumsSelectors.getRecentNumList(state),
});

export default connect(mapStateToProps)(Recommandations);
