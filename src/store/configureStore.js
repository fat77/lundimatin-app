import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {persistStore, persistReducer} from 'redux-persist';
// import storage from 'redux-persist/lib/storage'; // par défault vers AsyncStoragindexe

import AsyncStorage from '@react-native-community/async-storage';

// DUCKS
import recentNums from '../ducks/recentNums';
import favorites from '../ducks/favorites';
import themes from '../ducks/themes';
//import localImages from '../ducks/localImages';

// import rootSaga from '../sagas/rootSaga';
import rootSaga from '../sagas';

const persistConfig = {
  timeout: 10000,
  key: 'redux-persist',
  storage: AsyncStorage,
  blacklist: ['preloadedInSwiper'],
};

const rootReducer = combineReducers({
  recentNums,
  favorites,
  themes,
  // localImages,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const initialState = {
  themes: [],
  recentNums: {
    data: [],
    isFetching: false,
  },
  favorites: {
    data: [],
  },
  // localImages: {
  //   data: [],
  //   imagesToDownload: [],
  // },
};

const sagaMiddleware = createSagaMiddleware();

/* eslint-disable no-underscore-dangle */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
/* eslint-enable */

export const store = createStore(
  persistedReducer,
  initialState,
  composeEnhancers(applyMiddleware(sagaMiddleware)),
);

export const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);
