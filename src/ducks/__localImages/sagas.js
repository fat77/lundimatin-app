// @flow
import type { Saga } from 'redux-saga';
import {
  select,
  call,
  all,
  put,
  takeLeading,
  takeEvery,
  delay,
} from 'redux-saga/effects';

import actions, { types } from './actions';
import selectors from './selectors';

import type { localImageType, stateType } from '../../type';

import { favoritesSelectors } from '../favorites';
import { recentNumsSelectors } from '../recentNums';

import shared from '../shared';

import {
  downloadImageToFileSystem,
  removeImageFromFileSystem,
} from '../../fileSystem';


function* downloadImage(image: localImageType) {
  try {
    yield call(downloadImageToFileSystem, image.remoteSmall);
    yield put(actions.downloadSuccess(image));
  } catch {
    yield put(actions.downloadFailure(image));
  }
}

function* downloadImagesQueue() {
  const imagesInQueue = yield select(selectors.getImagesToDownload);
  if (imagesInQueue.length === 0) return;
  yield delay(500);
  yield call(downloadImage, imagesInQueue[0]);
}

function* removeImage(image: localImageType) {
  try {
    // everything after '?' in the path should be removed
    yield call(removeImageFromFileSystem, image.remoteSmall.split('?')[0]);
    yield put(actions.removeSuccess(image));
  } catch {
    yield put(actions.removeFailure(image));
  }
}

function* removeImagesQueue(imgArray: Array<localImageType>) {
  yield all(imgArray.map(image => call(removeImage, image)));
}

function* synchronizeImages() {
  // get image objects from both recent nums and favorites
  const favoritesImages: Array<localImageType> = yield select(
    favoritesSelectors.getFavoritesImageList,
  );
  const recentNumsImages: Array<localImageType> = yield select(
    recentNumsSelectors.getRecentImageList,
  );

  // keep only one of each in an array (by filtering on the name value)
  const recentNumsImagesNames: Array<String> = yield select(
    recentNumsSelectors.getRecentImagesLocalNames,
  );
  const newImages = [
    ...favoritesImages
      .filter(favImg => !recentNumsImagesNames.includes(favImg.name)),
    ...recentNumsImages,
  ];

  // compare localImages with newImages
  const localImages: Array<localImageType> = yield select(
    selectors.getLocalImages,
  );
  const imagesToRemove = localImages
    .filter(locImg => !newImages.map(img => img.name).includes(locImg.name));
  const imagesToDownload = newImages
    .filter(newImg => !localImages.map(img => img.name).includes(newImg.name));

  yield put(actions.setImagesToDownload(imagesToDownload));

  yield call(downloadImagesQueue);
  yield call(removeImagesQueue, imagesToRemove);
}

function* watchImagesSync() {
  yield takeLeading(shared.types.SYNC_IMAGES, synchronizeImages);
}

function* imagesQueueWatcher() {
  yield takeEvery([types.DOWNLOAD_SUCCESS, types.DOWNLOAD_FAILURE], downloadImagesQueue);
}

function* localImageSaga(): Saga<void> {
  yield all([
    call(watchImagesSync),
    call(imagesQueueWatcher),
  ]);
}

export default localImageSaga;
