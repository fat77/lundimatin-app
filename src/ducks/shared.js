const SYNC_IMAGES = 'lm/shared/SYNC_IMAGES';
const INITIALIZE_STATE = 'lm/shared/INITIALIZE_STATE';

const syncImages = () => ({
  type: SYNC_IMAGES,
});

const initializeState = () => ({
  type: INITIALIZE_STATE,
});

export default {
  types: { SYNC_IMAGES, INITIALIZE_STATE },
  actions: { syncImages, initializeState },
};
