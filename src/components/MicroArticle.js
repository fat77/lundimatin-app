// @flow
import React from 'react';
import {View, Image, TouchableOpacity, ActivityIndicator} from 'react-native';
import {Text, Icon, Button, ListItem} from 'native-base';
import {connect} from 'react-redux';
import HTML from 'react-native-render-html';
import moment from 'moment';

import type {articleType} from '../type';
import {favoritesSelectors, favoritesOperations} from '../ducks/favorites';
import {externalPath} from '../api/constants';
import {colors} from '../css/cssVariables';
import {isIOS} from '../utils/platform';

const getDate = (numDate: string) => {
  moment.locale('fr');
  const realDate = moment(numDate).format('Do MMM YY');
  return (
    <Text
      style={{
        paddingLeft: 7,
        paddingRight: 15,
        alignSelf: 'center',
        fontSize: 16,
        color: colors.blackLM,
      }}>
      {realDate}
    </Text>
  );
};

type Props = {
  article: articleType,
  onPress: () => void,
  favoriteList: Array<string>,
  toggleFavorite: (isFavorite: boolean, article: Object) => void,
};

const MicroArticle = ({
  article,
  onPress,
  favoriteList,
  toggleFavorite,
}: Props) => {
  const isFavorite = favoriteList.includes(article.id);
  const [isArticleLoading, setIsArticleLoading] = React.useState(false);
  // React.useEffect(() => {
  //   setIsArticleLoading(false);
  // }, []);

  const displayArticle = async () => {
    setIsArticleLoading(true);
    await onPress();
    setIsArticleLoading(false);
  };

  return (
    <ListItem style={{marginLeft: 0}}>
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={displayArticle}
        style={{flex: 1}}>
        <View
          style={{
            padding: 10,
            display: 'flex',
            flexDirection: 'row',

            alignItems: 'center',
          }}>
          <Image
            style={{
              flex: 1,
              marginRight: 10,
              marginLeft: 10,
              height: 100,
              maxWidth: 100,
            }}
            source={{uri: externalPath + article.logo_petit}}
          />
          {isArticleLoading ? (
            <ActivityIndicator
              style={{
                marginRight: 'auto',
                marginLeft: 'auto',
              }}
            />
          ) : (
            <View style={{flex: 2, marginRight: 10}}>
              <HTML
                html={`<h1>${article.titre}</h1>`}
                allowedStyles={[]}
                tagsStyles={{
                  h1: {
                    fontSize: 20,
                    lineHeight: 20,
                    fontWeight: isIOS ? null : 'bold',
                  },
                }}
              />
            </View>
          )}
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}>
          {/* <Button
            style={{paddingRight: 10}}
            small
            transparent
            onPress={() => toggleFavorite(isFavorite, article)}>
            {!isFavorite ? (
              <Icon
                style={{
                  fontSize: 16,
                  color: colors.blackLM,
                  alignSelf: 'center',
                }}
                type="FontAwesome"
                name="bookmark-o"
              />
            ) : (
              <Icon
                style={{fontSize: 16, color: colors.blackLM}}
                type="FontAwesome"
                name="bookmark"
              />
            )}
          </Button> */}
          <Icon
            style={{
              fontSize: 16,
              color: colors.blackLM,
              alignSelf: 'center',
            }}
            type="AntDesign"
            name="calendar"
          />
          {getDate(article.date_publication)}
        </View>
      </TouchableOpacity>
    </ListItem>
  );
};

const mapStateToProps = state => ({
  favorites: state.favorites,
  favoriteList: favoritesSelectors.getFavoriteList(state),
});

const mapDispatchToProps = dispatch => ({
  toggleFavorite: (isFavorite, article) =>
    dispatch(favoritesOperations.doToggleFavorite(isFavorite, article)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MicroArticle);
