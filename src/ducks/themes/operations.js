// @flow

import actions from './actions';

const doUpdateThemes = () => actions.updateRequest();
const doClearThemes = () => actions.clearData();

export default { doUpdateThemes, doClearThemes };
