// @flow

import React from 'react';
import {
  ScrollView,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import {withNavigation} from 'react-navigation';

import type {numeroType} from '../../type';

const styles = StyleSheet.create({
  container: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  article: {
    margin: 10,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  title: {
    textAlign: 'center',
    fontSize: 19,
    fontWeight: 'bold',
  },
  activeTitle: {
    color: 'red',
  },
});

type Props = {
  numero: numeroType,
  navigation: any,
};

const Numeros = ({numero, navigation}: Props) => (
  <ScrollView style={styles.container}>
    <Text style={styles.title}>{numero.numero}</Text>
    {numero.articles.map((article, index) => (
      <TouchableOpacity
        key={article.id}
        onPress={() =>
          navigation.navigate('Article', {
            articleIndex: index,
            numero,
          })
        }
        style={styles.article}>
        <Text>{article.titre}</Text>
        <Image
          source={{uri: article.localLogo}}
          style={{width: 200, height: 200}}
        />
      </TouchableOpacity>
    ))}
  </ScrollView>
);

export default withNavigation(Numeros);
