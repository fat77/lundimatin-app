// @flow

import React from 'react';
import {StyleSheet, Modal, TouchableOpacity, View, Text} from 'react-native';
import {Right, Left, ListItem, Radio} from 'native-base';

import {colors, sizes} from '../../css/cssVariables';
import {sansRegBlack} from '../../css/fonts';

const styles = StyleSheet.create({
  modalContent: {
    minHeight: 100,
    minWidth: 300,
    backgroundColor: colors.whiteLM,
  },
  touchable: {
    paddingTop: 50,
  },
  modalText: {
    ...sansRegBlack,
  },
  container: {
    height: sizes.fullheight,
  },
  btn: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
  },
});

type Props = {
  isModalOn: boolean,
  selector: string,
  displayModal: () => void,
  sortFav: (selector: string) => void,
};

const FavoritesModal = ({
  isModalOn,
  selector,
  displayModal,
  sortFav,
}: Props) => (
  <Modal
    animationType="fade"
    visible={isModalOn}
    transparent
    onRequestClose={displayModal}>
    <TouchableOpacity style={styles.touchable} onPressIn={displayModal}>
      <View style={styles.container}>
        <View style={styles.modalContent}>
          <ListItem>
            <TouchableOpacity
              style={styles.btn}
              onPress={() => sortFav('date-added')}>
              <Text style={styles.modalText}>
                trier par date d’ajout aux favoris
              </Text>

              <Radio
                color={colors.blackLM}
                selectedColor={colors.blackLM}
                selected={selector === 'date-added'}
              />
            </TouchableOpacity>
          </ListItem>
          <ListItem>
            <TouchableOpacity
              style={styles.btn}
              onPress={() => sortFav('date-publication')}>
              <Text style={styles.modalText}>
                trier par date de publication
              </Text>
              <Radio
                color={colors.blackLM}
                selectedColor={colors.blackLM}
                selected={selector === 'date-publication'}
                onPress={() => sortFav('date-publication')}
              />
            </TouchableOpacity>
          </ListItem>
        </View>
      </View>
    </TouchableOpacity>
  </Modal>
);

export default FavoritesModal;
