// @flow

import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  statusIndicator: {
    color: 'rgb(110, 117, 115)',
    alignItems: 'center',
    textAlign: 'center',
  },
});

type Props = {
  status: Object,
  logos: Object,
  images: Object,
};

const StatusIndicator = ({status, logos, images}: Props) => {
  switch (status.activity) {
    case 'downloadingNum': {
      return (
        <View>
          <Text style={styles.statusIndicator}>
            On vous télécharge des bons petits numéros !
          </Text>
          <Text style={styles.statusIndicator}>
            {`Numéro ${status.target} en cours ${logos.currents.length}/${logos
              .inQueue.length + logos.currents.length}`}
          </Text>
        </View>
      );
    }
    case 'downloadingDocs': {
      return (
        <Text style={styles.statusIndicator}>
          {`Téléchargement des images en cours ${
            images.currents.length
          }/${images.inQueue.length + images.currents.length}`}
        </Text>
      );
    }
    case 'checking': {
      return (
        <Text style={styles.statusIndicator}>
          On regarde si les articles sont à jours !
        </Text>
      );
    }
    default: {
      return <Text style={styles.statusIndicator}>.</Text>;
    }
  }
};

export default StatusIndicator;
