import reducer from './reducers';

export {default as localImagesSelectors} from './selectors';
export {default as localImagesOperations} from './operations';
export {types as localImagesTypes} from './actions';
export {default as localImagesSaga} from './sagas';

export default reducer;
