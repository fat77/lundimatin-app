// @flow
import {spipAPI} from './constants';

// types
import type {numerosType} from '../type';

// Path options
const numerosPath = `${spipAPI}page=api_numeros`;
const numeroPath = `${spipAPI}page=api_numero`;
const articlePath = `${spipAPI}page=api_article`;
const articlesPath = `${spipAPI}page=api_articles`;
const wordPath = `${spipAPI}page=api_mots`;
const slugPath = `${spipAPI}page=api_slug`;

const articleUrl = url => `&url=${url}`;
const articleId = id => `&id_article=${id}`;
const numeroId = id => `&numero=${id}`;
const nombre = number => `&nombre=${number}`;
const recalcul = '&var_mode=recalcul';
const groupe = word => {
  switch (word) {
    case 'themes':
      return '&id_groupe=7';
    default:
      return ' ';
  }
};
const tri = option => `&tri=${option}`;
const sensTri = boolean => `&sens_tri=${boolean}`;
const searchFor = search => `&recherche=${search}`;
const forWord = wordId => `&id_mot=${wordId}`;
const debut = number => `&debut=${number}`;

async function getLastNumeros() {
  const path = numerosPath + nombre(3) + recalcul;
  const response = await fetch(path);
  return (response.json(): Promise<numerosType>);
}

async function getNumero(id: string) {
  const path = numeroPath + numeroId(id) + recalcul;
  const response = await fetch(path);
  return response.json();
}

async function getArticle(id: string) {
  const path = articlePath + articleId(id) + recalcul;
  const response = await fetch(path);
  return response.json();
}

async function getArticleFromSlug(slug: string) {
  const path = slugPath + articleUrl(slug) + recalcul;
  const response = await fetch(path);
  return response.json();
}

async function getArticlesForSearch(search: string, startNb: number) {
  const path =
    articlesPath +
    tri('date_modif') +
    sensTri(1) +
    searchFor(search) +
    nombre(10) +
    debut(startNb);
  const response = await fetch(path);
  return response.json();
}

async function getThemes() {
  const path = wordPath + groupe('themes');
  const response = await fetch(path);
  return response.json();
}

async function getArticlesForWord(
  wordId: string,
  startNb: number,
  numberArticles?: number,
) {
  let path;
  if (numberArticles) {
    path =
      articlesPath +
      tri('date_modif') +
      sensTri(1) +
      forWord(wordId) +
      debut(startNb) +
      nombre(numberArticles);
  } else {
    path =
      articlesPath +
      tri('date_modif') +
      sensTri(1) +
      forWord(wordId) +
      debut(startNb);
  }
  const response = await fetch(path);

  return response.json();
}

async function getPopularArticles(startNb: number) {
  const path =
    articlesPath + tri('popularite') + sensTri(1) + nombre(10) + debut(startNb);
  const response = await fetch(path);
  return response.json();
}

export default {
  getNumero,
  getArticle,
  getLastNumeros,
  getThemes,
  getArticlesForSearch,
  getArticlesForWord,
  getPopularArticles,
  getArticleFromSlug,
};
