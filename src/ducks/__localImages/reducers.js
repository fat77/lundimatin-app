// @flow

import {combineReducers} from 'redux';

// importing flowTypes
import type {stateType} from '../../type';

// shared actions
import shared from '../shared';

// importing action types and flowTypes for actions
import {
  types,
  type DownloadSuccess,
  type DownloadFailure,
  type RemoveSuccess,
  type RemoveFailure,
  type ClearData,
  type SetImagesToDownload,
} from './actions';

// Defining Action type
type Action =
  | DownloadSuccess
  | DownloadFailure
  | RemoveSuccess
  | RemoveFailure
  | ClearData
  | SetImagesToDownload;

// Defining Reducer type
type localImagesType = $PropertyType<stateType, 'localImages'>;

// Reducer
function data(state: localImagesType = [], action: Action) {
  switch (action.type) {
    case types.DOWNLOAD_SUCCESS: {
      return [...state, action.payload.image];
    }
    case types.DOWNLOAD_FAILURE: {
      return state;
    }
    case types.REMOVE_SUCCESS: {
      const imgToRemove = action.payload.image.remoteSmall;
      const newArray: localImagesType = state.filter(
        image => imgToRemove !== image.remoteSmall,
      );
      return newArray;
    }
    case types.REMOVE_FAILURE: {
      return state;
    }
    case shared.types.INITIALIZE_STATE: {
      if (!Array.isArray(state)) {
        return [];
      }
      return state;
    }
    case types.CLEAR_DATA: {
      return [];
    }
    default: {
      return state;
    }
  }
}

function imagesToDownload(state: localImagesType = [], action: Action) {
  switch (action.type) {
    case types.SET_IMAGES_TO_DOWNLOAD: {
      return action.payload.images;
    }
    case types.DOWNLOAD_SUCCESS: {
      const imgToRemove = action.payload.image.remoteSmall;
      const newArray: localImagesType = state.filter(
        image => imgToRemove !== image.remoteSmall,
      );
      return newArray;
    }
    case types.CLEAR_DATA: {
      return [];
    }
    default: {
      return state;
    }
  }
}

export default combineReducers({data, imagesToDownload});
