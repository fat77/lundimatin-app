// @flow

import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import Share from 'react-native-share';
import {Icon} from 'native-base';
import {connect} from 'react-redux';
import Animated from 'react-native-reanimated';
import he from 'he';

// CSS
import {sizes, colors, fontsSizes} from '../../css/cssVariables';
import {artHeader} from '../../css/fonts';
// DUCKS
import {favoritesSelectors, favoritesOperations} from '../../ducks/favorites';
import {recentNumsSelectors} from '../../ducks/recentNums';

const {diffClamp, interpolate, multiply} = Animated;

const styles = StyleSheet.create({
  navBar: {
    color: colors.blackLM,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    width: sizes.fullWidth,

    //paddingBottom: 10,
    backgroundColor: colors.whiteLM,
    shadowColor: 'transparent',
    elevation: 1,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    height: 50,
    zIndex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  displayIcons: {
    //opacity: animatedTitleOpacity,
    flexDirection: 'row',
    position: 'absolute',
    right: 10,
  },
  favIcon: {
    color: colors.goldLM,
    fontSize: fontsSizes.navBar,
    marginHorizontal: 12,
  },
  shareIcon: {
    color: colors.blackLM,
    fontSize: fontsSizes.navBar,
    marginHorizontal: 12,
  },
  liseret: {
    position: 'absolute',
    top: 0,
    width: sizes.fullWidth,
    backgroundColor: colors.greyLM,
    opacity: 0.2,
    height: 2
  }
});

const onShare = async (id, htmlTitle) => {
  const titre = he.decode(htmlTitle) // Pour éviter les caractères spéciaux
  const shareOptions = {
    message: `"${titre}", sur lundi.am`,
    url: `https://lundi.am/${id}`,
  };
  Share.open(shareOptions);
};

type Props = {
  favoriteList: Array<string>,
  article: Object,
  scrollY: number,
  toggleFavorite: (isFavorite: boolean, article: Object) => void,
};

const HeaderArticle = ({
  toggleFavorite,
  favoriteList,
  article,
  scrollY,
}: Props) => {
  const isFavorite = favoriteList.includes(article.id);

  const diffClampNode = diffClamp(scrollY, 0, 50);
  const animatedNavBarTranslateY = multiply(diffClampNode, 1);
  const animatedTitleOpacity = interpolate(animatedNavBarTranslateY, {
    inputRange: [0, sizes.navBarHeight],
    outputRange: [1, -1],
    extrapolate: 'clamp',
  });
  

  return (
    <Animated.View
      style={[
        styles.navBar,
        {
          transform: [
            {
              translateY: animatedNavBarTranslateY,
            },
          ],
        },
      ]}>
      <View style={styles.liseret} />
      <Animated.View style={[styles.displayIcons, {opacity: animatedTitleOpacity}]}>
        <TouchableOpacity onPress={() => toggleFavorite(isFavorite, article)}>
          {!isFavorite ? (
            <Icon
              style={styles.favIcon}
              type="FontAwesome"
              name="bookmark-o"
            />
          ) : (
            <Icon
              style={styles.favIcon}
              type="FontAwesome"
              name="bookmark"
            />
          )}
        </TouchableOpacity>
        <TouchableOpacity onPress={() => onShare(article.id, article.titre)}>
          <Icon style={styles.shareIcon} type="SimpleLineIcons" name="share" />
        </TouchableOpacity>
      </Animated.View>
    </Animated.View>
  );
};

const mapStateToProps = state => ({
  favorites: state.favorites.data,
  favoriteList: favoritesSelectors.getFavoriteList(state),
});

const mapDispatchToProps = dispatch => ({
  toggleFavorite: (isFavorite, article) =>
    dispatch(favoritesOperations.doToggleFavorite(isFavorite, article)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderArticle);
