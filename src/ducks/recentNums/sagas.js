// @flow
import type { Saga } from 'redux-saga';
import {
  put,
  call,
  select,
  takeLeading,
  fork,
  delay,
} from 'redux-saga/effects';

import type { numerosType } from '../../type';

import shared from '../shared';

import actions, { types } from './actions';
import selectors from './selectors';

import API from '../../api/requests';

function* refreshNewArticles(prevArticleList, nextArticleList) {
  const newArticles = nextArticleList.filter(id => !prevArticleList.slice(20).includes(id)); // slice est là pour le test.
  yield put(actions.refreshNewArticles(newArticles));
  yield delay(30000);
  yield put(actions.clearNewArticles(newArticles));
}

function* numsUpdater() {
  const prevArticleList: Array<string> = yield select(selectors.getRecentArticleList);
  try {
    const lastNums: numerosType = yield call(API.getLastNumeros);
    yield put(actions.updateSuccess(lastNums));
    const nextArticleList: Array<string> = yield select(selectors.getRecentArticleList);
    // yield fork(refreshNewArticles, prevArticleList, nextArticleList);
    // yield put(shared.actions.syncImages());
  } catch (error) {
    yield put(actions.updateFailure(error));
  }
}

function* numsWatcher(): Saga<void> {
  yield takeLeading(types.UPDATE_REQUEST, numsUpdater);
}

export default numsWatcher;
