// @flow

import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import { Container, Content, Text, Button, Icon, Spinner } from 'native-base';
import type { NavigationScreenProp } from 'react-navigation';

import { colors } from '../css/cssVariables';
import API from '../api/requests';

import type { articleType, WordType } from '../type';
import HeaderWithBurger from './sharedWidgets/HeaderWithBurger';
import MicroArticle from './MicroArticle';
import { thisExpression } from '@babel/types';

const styles = StyleSheet.create({
  search: {
    marginTop: 20,
  },
  loading: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  textIndicator: {
    color: colors.greyLM,
    padding: 20,
  },
});

type Props = {
  navigation: NavigationScreenProp<{ params: { word: WordType } }>,
};

type State = {
  articles: ?Array<articleType>,
  isLoading: boolean,
  startAt: number,
  total: number,
  id: number,
  isLoadingNext: number,
  numberOfDisplayedArticles: number,
  numberArticleAtEachRequest: number,
};

class Word extends Component<Props, State> {
  static navigationOptions = {
    title: 'letitredelarecherche',
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      articles: [],
      isLoading: true,
      startAt: 0,
      total: 0,
      numberOfDisplayedArticles: 0,
      isLoadingNext: false,
      numberArticleAtEachRequest: 10, // On demande 10 articles à chaque requête à l'API
    };
  }

  componentDidMount = async () => {
    const { navigation } = this.props;
    const { id } = navigation.state.params.word;
    const { total } = await API.getArticlesForWord(id, 0);

    const { articles } = await API.getArticlesForWord(id, 0, //If less articles than requested
      total < this.state.numberArticleAtEachRequest ?
        total :
        this.state.numberArticleAtEachRequest
    );
    

    this.setState({ articles, isLoading: false, id, total, numberOfDisplayedArticles: this.state.numberArticleAtEachRequest });



  };

  componentDidUpdate = async (prevProps: Props) => {
    const prevWordId = prevProps.navigation.state.params.word.id;
    const { navigation } = this.props;
    const { id } = navigation.state.params.word;
    const { total } = await API.getArticlesForWord(id, 0);
    
    if (prevWordId !== id) {
      this.setState({ isLoading: true, startAt: 0 });
      const { articles } = await API.getArticlesForWord(id, 0, //If less total articles than requested
        total < this.state.numberArticleAtEachRequest ?
          total :
          this.state.numberArticleAtEachRequest
      );

      this.setState({ articles, isLoading: false, id, total, numberOfDisplayedArticles: this.state.numberArticleAtEachRequest });
    }
  }; 

  nextResults = async () => {

    const { numberOfDisplayedArticles, id, numberArticleAtEachRequest } = this.state;
    this.setState({ isLoadingNext: true });


    const { articles, total } = await API.getArticlesForWord(id, numberOfDisplayedArticles, 
      (total-numberOfDisplayedArticles) > numberArticleAtEachRequest ? 
      numberArticleAtEachRequest : //Si il reste moins d'articles que le nombre demandé, on demande ceux qui restent
      total-numberOfDisplayedArticles
      );

    this.setState((prevState) => ({
      startAt: prevState.numberOfDisplayedArticles,
      numberOfDisplayedArticles: prevState.numberOfDisplayedArticles + numberArticleAtEachRequest,
      articles: [...prevState.articles, ...articles],
      total,
      isLoadingNext: false,
    }));


  };

  render() {
    const { navigation } = this.props;
    const { titre } = navigation.state.params.word;
    const { articles, isLoading, total, startAt, numberArticleAtEachRequest, isLoadingNext, numberOfDisplayedArticles } = this.state;
    return (
      <Container>
        <HeaderWithBurger navigation={navigation} title={titre} />
        <Content>
          <View style={styles.search}>
            {isLoading ? (
              <View style={styles.loading}>
                <ActivityIndicator size="large" />
                <Text style={styles.textIndicator}>
                  {`Nous recherchons les articles pour le thème "${titre}"`}
                </Text>
              </View>
            ) : (
                <View>
                  {articles && articles.length
                    ? articles.map((article, index) => (
                      <MicroArticle
                        key={article.id}
                        article={article}
                        onPress={() =>
                          navigation.navigate('WordArticle', {
                            indexNum: index,
                            article,
                            swiperName: titre,
                          })
                        }
                      />
                    ))
                    : null}

                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'space-around',
                      flexDirection: 'row',
                      margin: 20,
                    }}>
                    {total > numberOfDisplayedArticles ? (
                      <Button dark transparent onPress={this.nextResults}>
                        {!isLoadingNext ? (
                          <Icon name="downcircle" type="AntDesign" />
                        ) : (
                            <Spinner color="black" />
                          )}
                      </Button>
                    ) : null}
                  </View>
                </View>
              )}
          </View>
        </Content>
      </Container>
    );
  }
}

export default Word;
