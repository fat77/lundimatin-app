// @flow
import actions from './actions';

const doUpdateNums = () => actions.updateRequest();
const doClearNums = () => actions.clearData();
const doRemoveFromNewArticles = (id: string) => actions.removeFromNewArticles(id);

export default { doUpdateNums, doClearNums, doRemoveFromNewArticles };
