// @flow
import React, {PureComponent} from 'react';
import {
  ImageBackground,
  View,
  TouchableOpacity,
  StyleSheet,
  Animated,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import {Text, Icon} from 'native-base';
import {connect} from 'react-redux';
import HTML from 'react-native-render-html';
import type {NavigationScreenProp} from 'react-navigation';

import {IMG} from '../../fileSystem';
import {externalPath} from '../../api/constants';
import {recentNumsSelectors} from '../../ducks/recentNums';
import {favoritesSelectors, favoritesOperations} from '../../ducks/favorites';
import {fonts} from '../../css/fonts';
import {colors} from '../../css/cssVariables';
import type {articleType} from '../../type';
import {isIOS} from '../../utils/platform';
var Color = require('color');

const styles = StyleSheet.create({
  article: {
    margin: 10,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  titre: {
    fontSize: 30,
    fontWeight: isIOS ? null : 'bold',
    color: 'rgb(255, 255, 255)',
    textTransform: 'uppercase',
    alignItems: 'center',
    textAlign: 'center',
  },
  soustitre: {
    fontSize: 15,
    color: 'rgb(255, 255, 255)',
    alignItems: 'center',
    textAlign: 'center',
    paddingTop: 15,
    paddingBottom: 30,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    opacity: 0.5,
  },
  imgbck: {
    width: '100%',
    minHeight: 250,
    position: 'relative',
  },
  iconDisplay: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
  },
  tagOff: {
    opacity: 0,
    paddingTop: 8,
    paddingBottom: 8,
    paddingRight: 10,
    paddingLeft: 10,
  },
  iconColor: {
    color: 'white',
    opacity: 0.4,
    fontSize: 18,
    fontFamily: fonts.sansSerifLight,
    fontWeight: isIOS ? null : 'normal',
  },
  lectureTime: {
    flexDirection: 'row',
    position: 'absolute',
    alignItems: 'center',
    bottom: 8,
    right: 10,
    marginTop: 7,
  },
  textInfo: {
    color: 'white',
    opacity: 0.4,
    fontSize: 18,
    fontFamily: fonts.sansSerifLight,
    fontWeight: isIOS ? null : 'normal',
  },
  blackCircle: {
    backgroundColor: colors.blackLM,
    borderRadius: 45 / 2,
    height: 45,
    width: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

type Props = {
  article: articleType,
  toggleFavorite: (isFavorite: boolean, article: Object) => void,
  isFavorite: boolean,
  // imageName: string,
  isNew: boolean,
  // isLocal: boolean,
  articleIndex: number,
  articles: Array<articleType>,
  swiperName: string,
  navigation: NavigationScreenProp<{}>,
  whereToNavigate: string,
  mapping: any,
};

type State = {
  bookmarkSize: any,
  bookmarkSpin: any,
};

class MiniArticle extends PureComponent<Props, State> {
  // static whyDidYouRender = true;

  constructor() {
    super();
    this.state = {
      bookmarkSize: new Animated.Value(1),
      bookmarkSpin: new Animated.Value(0),
    };
  }

  onPress = () => {
    const {
      navigation,
      articleIndex,
      articles,
      swiperName,
      whereToNavigate,
      mapping,
    } = this.props;
    navigation.navigate(whereToNavigate, {
      articleIndex,
      articles,
      swiperName,
      mapping,
    });
  };

  bookmarkAnimation = () => {
    Animated.sequence([
      Animated.timing(this.state.bookmarkSize, {
        toValue: 1.3,
        useNativeDriver: true,
        duration: 200,
      }),
      Animated.timing(this.state.bookmarkSize, {
        toValue: 1,
        useNativeDriver: true,
        duration: 200,
      }),
    ]).start();
  };

  render() {
    const {
      article,
      isFavorite,
      // isLocal,
      // imageName,
      isNew,
      toggleFavorite,
    } = this.props;

    // Plus d'image en local :
    const uri = externalPath + article.logo_petit;

    const AnimatedIcon = Animated.createAnimatedComponent(Icon);

    const spin = this.state.bookmarkSpin.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });

    //color
    const color = Color(`#${article.couleur}`);
    const colorFade = color.darken(0.5).fade(0.3);
    const inversedColor = color
      .rotate(200)
      .darken(0.5)
      .fade(0.4);
    const saturateInversedColor = color
      .saturate(0.8)
      .darken(0.1)
      .fade(0.3);

    return (
      <TouchableOpacity activeOpacity={0.8} onPress={this.onPress}>
        <ImageBackground source={{uri}} style={styles.imgbck}>
          {/* <View
            style={{...styles.overlay, backgroundColor: `#${article.couleur}`}}
          /> */}
          <LinearGradient
            style={styles.imgbck}
            useAngle={true}
            angle={Math.random() * 360}
            angleCenter={{x: 0.5, y: 0.5}}
            colors={[
              inversedColor,
              //colorFade,
              saturateInversedColor,
              inversedColor,
            ]}
            locations={[0, 0.3, 1]}>
            <View style={styles.iconDisplay}>
              {typeof article.auteurs !== 'undefined' &&
              article.auteurs.length !== 0 ? (
                article.auteurs.map(auteur =>
                  auteur.nom ? (
                    <HTML
                      key={article.id}
                      html={`<p>${auteur.nom}</p>`}
                      allowedStyles={[]}
                      tagsStyles={{
                        p: {
                          color: 'white',
                          opacity: 0.4,
                          fontSize: 18,
                          fontFamily: fonts.sansSerifLight,
                          fontWeight: isIOS ? null : 'normal',
                        },
                      }}
                    />
                  ) : (
                    <Text style={styles.tagOff}>auteur</Text>
                  ),
                )
              ) : (
                <Text style={styles.tagOff}>auteur</Text>
              )}
              {isNew && (
                <Icon name="new" type="Entypo" style={{color: 'white'}} />
              )}
              <TouchableOpacity
                style={{padding: 15}}
                onPressIn={() => {
                  this.bookmarkAnimation();
                }}
                onPress={() => toggleFavorite(isFavorite, article)}>
                {!isFavorite ? (
                  <AnimatedIcon
                    style={{
                      color: colors.goldLM,
                      transform: [
                        {scale: this.state.bookmarkSize},
                        {rotate: spin},
                      ],
                    }}
                    type="FontAwesome"
                    name="bookmark-o"
                  />
                ) : (
                  <AnimatedIcon
                    style={{
                      color: colors.goldLM,
                      transform: [
                        {scale: this.state.bookmarkSize},
                        {rotate: spin},
                      ],
                    }}
                    type="FontAwesome"
                    name="bookmark"
                  />
                )}
              </TouchableOpacity>
            </View>
            <View>
              {article.titre && (
                <HTML
                  style={styles.titre}
                  html={`<h1>${article.titre}</h1>`}
                  allowedStyles={[]}
                  tagsStyles={{
                    h1: {
                      fontWeight: isIOS ? null : 'normal',
                      fontFamily: fonts.titleBold,
                      textAlign: 'center',
                      fontSize: 32,
                      color: 'rgb(255, 255, 255)',
                      lineHeight: 32,
                      paddingBottom: 5,
                      paddingTop: 7,
                      paddingLeft: 10,
                      paddingRight: 10,
                    },
                  }}
                  alterData={node => {
                    const {data} = node;
                    return data.toUpperCase();
                  }}
                />
              )}
              {article.soustitre && article.soustitre.length > 0 ? (
                <HTML
                  style={styles.soustitre}
                  html={article.soustitre}
                  allowedStyles={[]}
                  tagsStyles={{
                    p: {
                      fontWeight: isIOS ? null : 'normal',
                      fontFamily: fonts.sansSerifLight,
                      textAlign: 'center',
                      fontSize: 18,
                      color: 'rgb(255, 255, 255)',
                      paddingBottom: 30,
                      marginHorizontal: 10,
                    },
                  }}
                />
              ) : null}
            </View>
            {article.temps_lecture !== '0 min' ? (
              <View style={styles.lectureTime}>
                <Icon style={styles.textInfo} type="EvilIcons" name="clock" />
                <Text style={styles.textInfo}>{article.temps_lecture}</Text>
              </View>
            ) : null}
          </LinearGradient>
        </ImageBackground>
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const {article} = ownProps;
  const isFavorite = favoritesSelectors
    .getFavoriteList(state)
    .includes(article.id);
  const isNew = recentNumsSelectors.getNewArticles(state).includes(article.id);
  // const localImagesNames = localImagesSelectors.getLocalImagesNames(state);
  // const imageName = article.logo_petit
  //   .split('/')
  //   .slice(-1)
  //   .toString();
  // const isLocal = localImagesNames.includes(imageName);
  return {
    isFavorite,
    // imageName,
    isNew,
    // isLocal,
  };
};

const mapDispatchToProps = dispatch => ({
  toggleFavorite: (isFavorite, article) =>
    dispatch(favoritesOperations.doToggleFavorite(isFavorite, article)),
});
const miniArticle = connect(mapStateToProps, mapDispatchToProps)(MiniArticle);
// miniArticle.whyDidYouRender = true ;
export default  miniArticle
