// @flow
import type {localImageType} from '../../type';

// Types

const DOWNLOAD_SUCCESS = 'lm/localImages/DOWNLOAD_SUCCESS';
export type DownloadSuccessType = 'lm/localImages/DOWNLOAD_SUCCESS';

const DOWNLOAD_FAILURE = 'lm/localImages/DOWNLOAD_FAILURE';
export type DownloadFailureType = 'lm/localImages/DOWNLOAD_FAILURE';

const REMOVE_SUCCESS = 'lm/localImages/REMOVE_SUCCESS';
export type RemoveSuccessType = 'lm/localImages/REMOVE_SUCCESS';

const REMOVE_FAILURE = 'lm/localImages/REMOVE_FAILURE';
export type RemoveFailureType = 'lm/localImages/REMOVE_FAILURE';

const CLEAR_DATA = 'lm/localImages/CLEAR_DATA';
export type ClearDataType = 'lm/localImages/CLEAR_DATA';

const SET_IMAGES_TO_DOWNLOAD = 'lm/localImages/SET_IMAGES_TO_DOWNLOAD';
export type SetImagesToDownloadType = 'lm/localImages/SET_IMAGES_TO_DOWNLOAD';

// Actions

export type DownloadSuccess = {
  type: DownloadSuccessType,
  payload: {image: localImageType},
};
const downloadSuccess = (image: localImageType): DownloadSuccess => ({
  type: DOWNLOAD_SUCCESS,
  payload: {image},
});

export type DownloadFailure = {
  type: DownloadFailureType,
  payload: {image: localImageType},
};
const downloadFailure = (image: localImageType): DownloadFailure => ({
  type: DOWNLOAD_FAILURE,
  payload: {image},
});

export type RemoveSuccess = {
  type: RemoveSuccessType,
  payload: {image: localImageType},
};
const removeSuccess = (image: localImageType): RemoveSuccess => ({
  type: REMOVE_SUCCESS,
  payload: {image},
});

export type RemoveFailure = {
  type: RemoveFailureType,
  payload: {image: localImageType},
};
const removeFailure = (image: localImageType): RemoveFailure => ({
  type: REMOVE_FAILURE,
  payload: {image},
});

export type ClearData = {type: ClearDataType};
const clearData = (): ClearData => ({
  type: CLEAR_DATA,
});

export type SetImagesToDownload = {
  type: SetImagesToDownloadType,
  payload: {images: Array<localImageType>},
};
const setImagesToDownload = (
  images: Array<localImageType>,
): SetImagesToDownload => ({
  type: SET_IMAGES_TO_DOWNLOAD,
  payload: {images},
});

export default {
  downloadSuccess,
  downloadFailure,
  removeFailure,
  removeSuccess,
  clearData,
  setImagesToDownload,
};

export const types = {
  DOWNLOAD_SUCCESS,
  DOWNLOAD_FAILURE,
  REMOVE_SUCCESS,
  REMOVE_FAILURE,
  CLEAR_DATA,
  SET_IMAGES_TO_DOWNLOAD,
};
