import React, {Component} from 'react';
import {StyleSheet, View, Text, ActivityIndicator} from 'react-native';
import {Root, StyleProvider} from 'native-base';
import getTheme from '../native-base-theme/components';
import platform from '../native-base-theme/variables/platform';
import {isAndroid} from './utils/platform';
import {connect} from 'react-redux';
// import { AppLoading } from 'expo';
import SplashScreen from 'react-native-splash-screen';
import * as Font from 'expo-font';
import {fonts} from './css/fonts';

StyleSheet.setStyleAttributePreprocessor('fontFamily', Font.processFontFamily);

// import registerToNotification from './src/features/registerToNotification';

// NAVIGATION
import NavigatorContainer from './components/navigation/AppNavigator';

// FILESYSTEM
import {checkFSDirectoryCreation} from './fileSystem';

// ACTIONS
import {recentNumsOperations} from './ducks/recentNums';
import {themesOperations} from './ducks/themes';
import shared from './ducks/shared';

// FONTS OBJECT
const fontsToLoad = {
  Roboto: require('../node_modules/native-base/Fonts/Roboto.ttf'),
  Roboto_medium: require('../node_modules/native-base/Fonts/Roboto_medium.ttf'),

  Ionicons: require('../node_modules/native-base/Fonts/Ionicons.ttf'),
  AntDesign: require('../node_modules/native-base/Fonts/AntDesign.ttf'),
  Entypo: require('../node_modules/native-base/Fonts/Entypo.ttf'),
  EvilIcons: require('../node_modules/native-base/Fonts/EvilIcons.ttf'),
  FontAwesome: require('../node_modules/native-base/Fonts/FontAwesome.ttf'),
  Feather: require('../node_modules/native-base/Fonts/Feather.ttf'),
  Entypo: require('../node_modules/native-base/Fonts/Entypo.ttf'),
  Foundation: require('../node_modules/native-base/Fonts/Foundation.ttf'),
  Ionicons: require('../node_modules/native-base/Fonts/Ionicons.ttf'),
  AntDesign: require('../node_modules/native-base/Fonts/AntDesign.ttf'),
  Entypo: require('../node_modules/native-base/Fonts/Entypo.ttf'),
  MaterialIcons: require('../node_modules/native-base/Fonts/MaterialIcons.ttf'),
  MaterialCommunityIcons: require('../node_modules/native-base/Fonts/MaterialCommunityIcons.ttf'),
  Octicons: require('../node_modules/native-base/Fonts/Octicons.ttf'),
  SimpleLineIcons: require('../node_modules/native-base/Fonts/SimpleLineIcons.ttf'),

  // 'neuton-regular': require('../assets/fonts/Neuton/Neuton-Regular.ttf'),
  // 'neuton-extra-bold': require('../assets/fonts/Neuton/Neuton-ExtraBold.ttf'),
  // 'neuton-bold': require('../assets/fonts/Neuton/Neuton-Bold.ttf'),
  // 'neuton-italic': require('../assets/fonts/Neuton/Neuton-Italic.ttf'),

  'alegreya-extra-bold': require('../assets/fonts/Alegreya/Alegreya-ExtraBold.ttf'),

  'open-sans-regular': require('../assets/fonts/Open_Sans/OpenSans-Regular.ttf'),
  'open-sans-light': require('../assets/fonts/Open_Sans/OpenSans-Light.ttf'),
  'open-sans-bold': require('../assets/fonts/Open_Sans/OpenSans-Bold.ttf'),
  'open-sans-italic': require('../assets/fonts/Open_Sans/OpenSans-Italic.ttf'),
  'open-sans-bold-italic': require('../assets/fonts/Open_Sans/OpenSans-BoldItalic.ttf'),

  'merriweather-regular': require('../assets/fonts/Merriweather/Merriweather-Regular.ttf'),
  'merriweather-italic': require('../assets/fonts/Merriweather/Merriweather-Italic.ttf'),
  'merriweather-bold': require('../assets/fonts/Merriweather/Merriweather-Bold.ttf'),
  'merriweather-bold-italic': require('../assets/fonts/Merriweather/Merriweather-BoldItalic.ttf'),
  'merriweather-light': require('../assets/fonts/Merriweather/Merriweather-Light.ttf'),
  'merriweather-light-italic': require('../assets/fonts/Merriweather/Merriweather-LightItalic.ttf'),

  'pt-sans-regular': require('../assets/fonts/PTSans/PTSans-Regular.ttf'),
  'pt-sans-bold': require('../assets/fonts/PTSans/PTSans-Bold.ttf'),
};

class AppContainer extends Component {
  state = {
    loading: true,
  };

  async componentDidMount() {
    // do stuff while splash screen is shown
    // After having done stuff (such as async tasks) hide the splash screen
    const {updateNums, updateThemes, initializeState} = this.props;
    // await checkFSDirectoryCreation();
    if (isAndroid) {
      await Font.loadAsync(fontsToLoad);
    }
    initializeState();
    updateNums();
    updateThemes();
    this.setState({loading: false});
    SplashScreen.hide();
  }

  render() {
    const {loading} = this.state;
    if (loading === true) {
      return (
        <Root>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator style={{marginBottom: 20}} size={'large'} />
            <Text style={{fontFamily: fonts.textRegular, fontSize: 18}}>
              Chargement ;-)
            </Text>
          </View>
        </Root>
      );
    }
    return (
      <Root>
        <StyleProvider style={getTheme(platform)}>
          <NavigatorContainer />
        </StyleProvider>
      </Root>
    );
  }
}

const mapStateToProps = state => ({
  recentNums: state.recentNums.data,
});

const mapDispatchToProps = dispatch => ({
  updateNums: () => dispatch(recentNumsOperations.doUpdateNums()),
  updateThemes: () => dispatch(themesOperations.doUpdateThemes()),
  initializeState: () => dispatch(shared.actions.initializeState()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
