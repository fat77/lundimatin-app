// @flow
import {LayoutAnimation, Linking} from 'react-native';
import * as WebBrowser from 'expo-web-browser';
import {isIOS} from '../../utils/platform';

// OPEN LINK
export function openLink(evt: any, href: string) {
  if (isIOS) {
    Linking.canOpenURL(href).then(supported => {
      if (supported) {
        Linking.openURL(href);
      }
    });
  } else {
    WebBrowser.openBrowserAsync(href);
  }
}

// //  SCROLL TO TOP

export const goToTop = (scroll: any) => {
  scroll.scrollTo({x: 0, y: 0, animated: true});
};

export const displayButton = (
  event: any,
  isScrollTopButtonVisible: boolean,
  viewOffset: number,
) => {
  let newViewOffset: number = viewOffset;
  const CustomLayoutLinear = {
    duration: 10,
    create: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.opacity,
    },
    update: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.opacity,
    },
    delete: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.opacity,
    },
  };
  const currentOffset = event.nativeEvent.contentOffset.y;
  const direction =
    currentOffset > 0 && currentOffset > newViewOffset ? 'down' : 'up';
  const isScrollingDown = direction === 'down';
  if (isScrollingDown !== isScrollTopButtonVisible) {
    LayoutAnimation.configureNext(CustomLayoutLinear);
  }
  newViewOffset = currentOffset;
  return {isScrollingDown, newViewOffset};
};
