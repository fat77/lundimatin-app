import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import * as Font from 'expo-font';
import {StyleSheet, View, Image, Text} from 'react-native';
// import AppIntroSlider from 'react-native-app-intro-slider';
import Introduction from './src/Introduction';
import {AsyncStorage} from 'react-native';
// REDUX
import {store, persistor} from './src/store/configureStore';
// NAVIGATION
import AppContainer from './src/AppContainer';
import {colors, sizes} from './src/css/cssVariables';

StyleSheet.setStyleAttributePreprocessor('fontFamily', Font.processFontFamily);

// WhyDidYouRender
// if (process.env.NODE_ENV !== 'production') {
//   const whyDidYouRender = require('@welldone-software/why-did-you-render');
//   whyDidYouRender(React);
// }

export default class App extends React.Component {
  state = {
    showIntro: null,
  };

  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem('showIntro');
      this.setState({
        showIntro: value === 'false' ? false : true,
        // showIntro: true,
      });
    } catch (e) {
      console.log(e);
    }
  }

  onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    this.setState({showIntro: false});
    const storeData = async () => {
      try {
        await AsyncStorage.setItem('showIntro', 'false');
      } catch (e) {
        // saving failed
      }
    };
    storeData();
  };

  render() {
    if (!this.state.showIntro) {
      return (
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <AppContainer />
          </PersistGate>
        </Provider>
      );
    } else {
      return <Introduction onDone={this.onDone} />;
    }
  }
}
