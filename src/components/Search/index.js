// @flow

import React, {Component} from 'react';
import {
  BackHandler,
  ScrollView,
  View,
  StyleSheet,
  UIManager,
  Platform,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import {
  Container,
  Button,
  Text,
  Input,
  Header,
  Item,
  Icon,
  Spinner,
} from 'native-base';
import type {NavigationScreenProp} from 'react-navigation';

import {colors} from '../../css/cssVariables';
import {fonts} from '../../css/fonts';

import type {articleType} from '../../type';

import API from '../../api/requests';
import {displayButton} from '../sharedWidgets/functions';

import ScrollUp from '../sharedWidgets/ScrollUp';
import HeaderWithBurger from '../sharedWidgets/HeaderWithBurger';
import MicroArticle from '../MicroArticle';

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

type State = {
  title: string,
  searchedText: string,
  isScrollTopButtonVisible: boolean,
  textInput: string,
  isLoadingNext: boolean,
  isSearching: boolean,
  articles: Array<articleType>,
  startAt: number,
  total: number,
  isInputFocused: boolean,
};

type Props = {
  navigation: NavigationScreenProp<{
    params: {articleIndex: number, article: articleType, swiperName: string},
  }>,
};

class Search extends Component<Props, State> {
  static navigationOptions = {
    title: 'letitredelarecherche',
  };
  backHandlerSearch: any;
  constructor(props: Props) {
    super(props);
    this.state = {
      title: 'Rechercher',
      textInput: '',
      searchedText: '',
      isLoadingNext: false,
      isSearching: false,
      articles: [],
      total: 0,
      startAt: 0,
      isInputFocused: false,
      isScrollTopButtonVisible: false,
    };
    this.offSet = 0;
    this.inputRef = React.createRef();

    if (
      Platform.OS === 'android' &&
      UIManager.setLayoutAnimationEnabledExperimental
    ) {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  componentDidMount = async () => {
    this.setState({isSearching: true});
    const {articles, total} = await API.getPopularArticles(0);
    this.setState({
      articles,
      isSearching: false,
      total: parseInt(total, 10),
    });

    this.backHandlerSearch = BackHandler.addEventListener(
      'hardwareBackPress',
      () => {
        //On rajoute une événement pour le clique sur la flèche retour Hardware

        if (this.state.isInputFocused === true) {
          //Si le champ rechercher est focus
          this.setState({isInputFocused: false});
          this.inputRef.current.wrappedInstance.blur(); //On défocuse l'input
          return true; // On prévient l'attitude par défaut
        }
      },
    );
  };

  componentWillUnmount() {
    this.backHandlerSearch.remove();
  }

  requestApi = async (searchedText: string, startAt: number) => {
    if (searchedText.length > 1) {
      const {articles, total} = await API.getArticlesForSearch(
        searchedText,
        startAt,
      );
      return {
        articles,
        total: parseInt(total, 10),
      };
    }
    const {articles, total} = await API.getPopularArticles(startAt);
    return {
      articles,
      total: parseInt(total, 10),
    };
  };

  startNewSearch = async () => {
    const {textInput} = this.state;
    Keyboard.dismiss();
    this.setState({isSearching: true});

    const {articles, total} = await this.requestApi(textInput, 0);
    this.setState({
      isSearching: false,
      articles,
      startAt: 0,
      searchedText: textInput,
      total: parseInt(total, 10),
    });
  };

  nextResults = async () => {
    const {startAt, searchedText} = this.state;
    this.setState({isLoadingNext: true});
    const {articles, total} = await this.requestApi(searchedText, startAt + 10);
    this.setState(prevState => ({
      isLoadingNext: false,
      articles: [...prevState.articles, ...articles],
      startAt: prevState.startAt + 10,
      total: parseInt(total, 10),
    }));
  };

  clearSearch = async () => {
    //Quand on appuie sur la croix
    this.setState({textInput: ''}); //On vide l'input
    this.inputRef.current.wrappedInstance.focus();

    const {articles, total} = await API.getPopularArticles(0); //On réaffiche les articles les plus populaires
    this.setState({
      articles,
      isSearching: false,
      searchedText: '',
      total: parseInt(total, 10),
    });
  };

  onInputFocus = () => {
    //Quand on focus sur l'input
    this.setState({isInputFocused: true});
  };

  onInputBlur = () => {
    //Quand on sort de l'input
    this.setState({isInputFocused: false});
  };

  // onScrollDown = (event: any) => {
  //   const { isScrollTopButtonVisible } = this.state;
  //   const isScrollingDown = displayButton(event, isScrollTopButtonVisible);
  //   if (isScrollingDown !== isScrollTopButtonVisible) {
  //     this.setState({
  //       isScrollTopButtonVisible: isScrollingDown,
  //     });
  //   }
  // };

  onScrollDown = (e: any) => {
    const {isScrollTopButtonVisible} = this.state;
    const scroll = displayButton(e, isScrollTopButtonVisible, this.offSet);
    if (scroll.isScrollingDown !== isScrollTopButtonVisible) {
      this.setState({
        isScrollTopButtonVisible: scroll.isScrollingDown,
      });
    }
    this.offSet = scroll.newViewOffset;
  };

  searchTextInputChange(text: string) {
    this.setState({textInput: text});
  }

  // FlowType
  scroll: any;

  inputRef: {current: any};

  offSet: number;

  render() {
    const {navigation} = this.props;
    const {
      articles,
      searchedText,
      title,
      startAt,
      total,
      isLoadingNext,
      isSearching,
      textInput,
      isInputFocused,
      isScrollTopButtonVisible,
    } = this.state;
    const swiperName =
      searchedText.length > 0 ? `Rechercher` : 'Articles populaires';
    return (
      <Container>
        <HeaderWithBurger navigation={navigation} title={title} />
        <Header
          androidStatusBarColor="#000000"
          searchBar
          autoCorrect={false}
          style={{backgroundColor: colors.blackLM}}>
          <Item>
            <Icon name="search" />
            <Input
              onChangeText={text => this.searchTextInputChange(text)}
              placeholder="Rechercher"
              returnKeyType="search"
              clearButtonMode="unless-editing"
              onSubmitEditing={this.startNewSearch}
              value={textInput}
              ref={this.inputRef}
              onFocus={this.onInputFocus}
              onBlur={this.onInputBlur}
            />
            {textInput.length > 0 && (
              <TouchableOpacity onPress={this.clearSearch}>
                <Icon name="close" type="AntDesign" />
              </TouchableOpacity>
            )}
          </Item>
        </Header>
        <ScrollView
          ref={c => {
            this.scroll = c;
          }}
          onScroll={this.onScrollDown}>
          <View>
            {isSearching ? (
              <View style={styles.loading}>
                <Spinner color="black" style={{marginTop: -10}} />
              </View>
            ) : !!articles.length ? (
              <View>
                {searchedText.length > 0 ? (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      margin: 10,
                    }}>
                    <Text>{`${Math.min(
                      startAt + 10,
                      total,
                    )} résultats sur ${total}`}</Text>
                  </View>
                ) : (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      margin: 10,
                    }}>
                    <Text
                      style={{
                        fontSize: 18,
                        fontFamily: fonts.sansSerifRegular,
                      }}>
                      Articles populaires:
                    </Text>
                  </View>
                )}
                <View>
                  {articles.map((article, index) => (
                    <MicroArticle
                      key={article.id}
                      article={article}
                      onPress={() =>
                        navigation.navigate('SearchArticle', {
                          indexNum: index,
                          article,
                          swiperName,
                        })
                      }
                    />
                  ))}
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'space-around',
                      flexDirection: 'row',
                      margin: 20,
                    }}>
                    {total - startAt > 10 ? (
                      <Button dark transparent onPress={this.nextResults}>
                        {!isLoadingNext ? (
                          <Icon name="downcircle" type="AntDesign" />
                        ) : (
                          <Spinner color="black" />
                        )}
                      </Button>
                    ) : null}
                  </View>
                </View>
              </View>
            ) : (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  margin: 10,
                }}>
                <Text>Aucun résultat</Text>
              </View>
            )}
          </View>
          {isInputFocused && (
            <View
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                backgroundColor: 'rgba(255, 255, 255, 0.9)',
              }}
            />
          )}
        </ScrollView>
        {isScrollTopButtonVisible ? (
          <ScrollUp
            isScrollTopButtonVisible={isScrollTopButtonVisible}
            scroll={this.scroll}
          />
        ) : null}
      </Container>
    );
  }
}

export default Search;
