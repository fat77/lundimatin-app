import actions from './actions';

function doClearImages() {
  return actions.clearData();
}

export default { doClearImages };
