// @flow
import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Platform,
  UIManager,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import Animated from 'react-native-reanimated';

import {Icon, Text} from 'native-base';
import {connect} from 'react-redux';
import type {NavigationScreenProp} from 'react-navigation';

// CSS
import {scrollUp, colors, sizes} from '../../css/cssVariables';
import {fonts} from '../../css/fonts';

import {favoritesSelectors} from '../../ducks/favorites';
import type {articleType} from '../../type';
import {displayButton} from '../sharedWidgets/functions';
import {externalPath} from '../../api/constants';

// COMPONENTS
import FooterArticle from './FooterArticle';
import LogoAndTitle from './LogoAndTitle';
import SoustitreAndChapo from './SoustitreAndChapo';
import TextArticle from './TextArticle';
import Recommandations from './Recommandations';
import Skeleton from './Skeleton';
// import ScrollUp from '../sharedWidgets/ScrollUp';
import BottomModal from './BottomModal';

const styles = StyleSheet.create({
  scrollViewContent: {
    backgroundColor: colors.whiteLM,
  },
  notes: {
    paddingTop: 35,
  },
  relativeNavBar: {
    color: colors.blackLM,
    paddingTop: sizes.statusBarHeight,
    marginTop: 10,
    backgroundColor: colors.whiteLM,
    shadowColor: 'transparent',
    height: sizes.navBarHeight,

    // elevation: 1,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    //height: sizes.navBarHeight,
    zIndex: 2,
    alignItems: 'center',
  },
});

type Props = {
  navigation: NavigationScreenProp<>,
  articles: Array<mixed>,
  article: articleType,
  indexNum: number,
  // swiperName: string,
  navigation: any,
  onArticleScroll: () => void,
  scrollY: any,
  mapping: any[],
};

type State = {
  loaded: boolean,
  isScrollTopButtonVisible: boolean,
  isModal: {showModal: boolean, modalFrom: string},
  requiredNote: null | string,
  modalDownload: boolean,
  downloadText: string,
};

class Article extends Component<Props, State> {
  static navigationOptions = {
    title: 'Article',
  };

  constructor(props) {
    super(props);
    this.offSet = 0;
    // this.scrollY = new Value(0);
    this.state = {
      loaded: true,
      isScrollTopButtonVisible: false,
      isModal: {showModal: false, modalFrom: ''},
      requiredNote: null,
      modalDownload: false,
      downloadText: '',
    };
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  setModalDownload = (text: string) => {
    this.setState({downloadText: text});
  };

  setIsModal = (bool: boolean, from: string) => {
    const newModal = {showModal: bool, modalFrom: from};
    this.setState({isModal: newModal});
  };

  setRequiredNote = (id: string) => {
    this.setState({requiredNote: id});
  };

  onScrollDown = e => {
    const {isScrollTopButtonVisible} = this.state;
    const scroll = displayButton(e, isScrollTopButtonVisible, this.offSet);
    if (scroll.isScrollingDown !== isScrollTopButtonVisible) {
      this.setState({
        isScrollTopButtonVisible: scroll.isScrollingDown,
      });
    }
    this.offSet = scroll.newViewOffset;
  };

  goToTop = scroll => {
    scroll.getNode().scrollTo({x: 0, y: 0, animated: true});
    this.setState({
      isScrollTopButtonVisible: false,
    });
  };

  // FlowType
  scroll: any;

  scrollY: any;

  offSet: number;

  render() {
    const {article, navigation, onArticleScroll, scrollY} = this.props;

    const {
      loaded,
      isScrollTopButtonVisible,
      requiredNote,
      isModal,
      modalDownload,
      downloadText,
    } = this.state;

    const images = [
      {
        // logo
        name: article.logo_petit
          .split('/')
          .slice(-1)
          .toString(),
        remoteBig: externalPath + article.logo,
        remoteSmall: externalPath + article.logo_petit,
        height: article.logo_petit_hauteur,
        width: article.logo_petit_largeur,
      },
      ...article.images.map(
        // all images
        image => ({
          name: image.url_petit
            .split('/')
            .slice(-1)
            .toString(),
          remoteBig: externalPath + image.url,
          remoteSmall: externalPath + image.url_petit,
          width: image.largeur,
          height: image.hauteur,
        }),
      ),
    ];
    return (
      <SafeAreaView>
        <Animated.ScrollView
          style={styles.scrollViewContent}
          scrollEventThrottle={1000}
          ref={c => {
            this.scroll = c;
          }}
          onScroll={onArticleScroll}
          onMomentumScrollEnd={this.onScrollDown}>
          <LogoAndTitle
            article={article}
            images={images}
            navigation={navigation}
          />
          <SoustitreAndChapo
            article={article}
            setIsModal={this.setIsModal}
            setRequiredNote={this.setRequiredNote}
            requiredNote={requiredNote}
            navigation={navigation}
            setModalDownload={this.setModalDownload}
          />
          {loaded ? (
            <View>
              <TextArticle
                article={article}
                images={images}
                navigation={navigation}
                setIsModal={this.setIsModal}
                setRequiredNote={this.setRequiredNote}
                requiredNote={requiredNote}
                setModalDownload={this.setModalDownload}
              />
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 50,
                }}>
                <Text
                  style={{
                    fontSize: 18,
                    fontFamily: fonts.sansSerifregular,
                  }}>
                  Articles recommandés
                </Text>
              </View>

              <Recommandations article={article} navigation={navigation} />
            </View>
          ) : (
            <View style={{margin: 10}}>
              <Skeleton numberFromProps={60} />
            </View>
          )}
          <View style={{height: sizes.navBarHeight}} />
        </Animated.ScrollView>
        {isScrollTopButtonVisible ? (
          <TouchableOpacity
            activeOpacity={1}
            style={scrollUp.button}
            onPress={() => this.goToTop(this.scroll)}>
            <Icon style={scrollUp.icon} type="AntDesign" name="upcircleo" />
          </TouchableOpacity>
        ) : null}

        {/* MODAL TO NOTIFY WHEN A FILE IS DOWNLOADED */}
        {/* AND */}
        {/* MODAL POUR LES NOTES */}

        <BottomModal
          isModal={isModal}
          setIsModal={this.setIsModal}
          modalDownload={modalDownload}
          downloadText={downloadText}
          requiredNote={requiredNote}
          article={article}
          navigation={navigation}
        />

        <FooterArticle article={article} scrollY={scrollY} />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  favorites: state.favorites.data,
  favoriteList: favoritesSelectors.getFavoriteList(state),
});

export default connect(mapStateToProps)(Article);
