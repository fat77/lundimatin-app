import React from 'react';
import {
  ScrollView,
  Modal,
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
} from 'react-native';
import {Icon} from 'native-base';
import openUrlInApp from '../../link';
import {openLink} from '../sharedWidgets/functions';
import {fontsSizes, colors} from '../../css/cssVariables';
import {fonts} from '../../css/fonts';
import HTML from 'react-native-render-html';
import type {articleType} from '../../type';
import type {NavigationScreenProp} from 'react-navigation';

const styles = StyleSheet.create({
  invisibleCloseView: {
    zIndex: 0,
    width: '100%',
    height: '100%',
  },
  mainViewForDownloadModal: {
    backgroundColor: colors.goldLM,
    position: 'absolute',
    width: '100%',
    bottom: -1,
    padding: 20,
    paddingTop: 40,
  },
  mainViewForNoteModal: {
    backgroundColor: colors.greyDarkerLM,
    position: 'absolute',
    width: '100%',
    bottom: -1,
    padding: 20,
    paddingTop: 40,
  },
  closeButton: {
    position: 'absolute',
    top: 10,
    right: 10,
    // paddingTop: 35,
    zIndex: 999,
  },
  closeIcon: {
    color: colors.whiteLM,
    backgroundColor: 'rgba(255,255,255,0)',
  },
  text: {
    textAlign: 'justify',
    fontFamily: fonts.textLight,
    color: colors.whiteLM,
    fontSize: fontsSizes.bodyArticle - 2,
    marginVertical: 2,
  },
});

type Props = {
  isModal: {showModal: boolean, modalFrom: string},
  downloadText: string,
  setIsModal: (x: boolean, y: string) => void,
  requiredNote?: null | string,
  article: articleType,
  navigation: NavigationScreenProp<>,
};

export default function BottomModal({
  downloadText,
  setIsModal,
  isModal,
  requiredNote,
  article,
  navigation,
}: Props) {
  // liens vers l'app ou vers le dehors
  const open = (evt, href) => {
    const route = href.replace(/.*?:\/\//g, '');
    const routeName = route.split('/')[0];
    if (routeName === 'lundi.am/') {
      openUrlInApp({url: href}, navigation);
    } else {
      openLink(evt, href);
    }
  };

  return (
    <Modal
      animationType="slide"
      visible={isModal.showModal}
      transparent={true}
      onRequestClose={() => setIsModal(false, '')}>
      <TouchableOpacity
        style={styles.invisibleCloseView}
        onPress={() => {
          setIsModal(false, '');
        }}
      />
      <View
        style={
          isModal.modalFrom === 'note modal'
            ? styles.mainViewForNoteModal
            : styles.mainViewForDownloadModal
        }>
        <TouchableOpacity
          style={styles.closeButton}
          onPressIn={() => setIsModal(false, '')}>
          <Icon type="AntDesign" name="close" style={styles.closeIcon} />
        </TouchableOpacity>
        {isModal.modalFrom === 'note modal' ? (
          <ScrollView
            // style={{marginHorizontal: 20, marginRight: 42, }}
            showsVerticalScrollIndicator={false}>
            {/* <Text
              style={{
                fontWeight: 'normal',
                fontFamily: fonts.textBold,
                fontSize: 20,
                alignSelf: 'center',
                paddingTop: 70,
                paddingBottom: 30,
              }}>
              Notes:
            </Text> */}

            <HTML
              textSelectable
              html={article.notes}
              allowedStyles={[]}
              onLinkPress={(evt, href) => {
                open(evt, href);
              }}
              tagsStyles={{
                p: {
                  textAlign: 'justify',
                  fontFamily: fonts.textLight,
                  color: colors.whiteLM,
                  fontSize: fontsSizes.bodyArticle - 2,
                  marginVertical: 2,
                },
                span: {
                  fontFamily: fonts.textBold,
                },
                a: {
                  color: colors.whiteLM,
                  fontFamily: fonts.textLight,
                },
                i: {
                  fontStyle: 'normal',
                  fontFamily: fonts.textLightItalic,
                  color: colors.whiteLM,
                  fontSize: fontsSizes.bodyArticle - 2,
                },
              }}
              classesStyles={{
                italic: {
                  fontStyle: 'normal',
                  fontFamily: fonts.textLightItalic,
                  color: colors.whiteLM,
                  fontSize: fontsSizes.bodyArticle - 2,
                },
              }}
              alterNode={node => {
                const {name, parent, attribs} = node;
                if (attribs && attribs.id) {
                  const array = attribs.id.split('-');
                  let key;
                  if (array.length > 1) {
                    key = array[1];
                  } else {
                    key = attribs.id.match(/nb(\d+)/)[1];
                  }
                  // si contient nb + un nombre + "-" + un nombre, renvoie l'expression[0] et la clé[1]
                  // const [matchedName, key] = attribs.id.match(/nb\d+-(\d+)/);
                  if (key !== requiredNote) {
                    return true; // retourner false (ou null) laisserait le noeud intact
                  }
                }

                // retirer le link du numéro de note (surlignage, etc)
                if (attribs && attribs.class === 'spip_note') {
                  node.name = 'span';
                  return node;
                }

                if (name === 'i' && parent && parent.name === 'i') {
                  node.attribs = {class: 'italic'};
                  return node;
                }
              }}
            />
          </ScrollView>
        ) : (
          <Text style={styles.text}>
            {downloadText === ''
              ? 'Votre fichier a été téléchargé'
              : downloadText}
          </Text>
        )}
      </View>
    </Modal>
  );
}
