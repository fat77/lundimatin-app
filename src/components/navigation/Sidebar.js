// @flow

import React from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions,
  Linking,
} from 'react-native';
import {Container, Content, Text, Accordion, H1, Icon} from 'native-base';
import {Row, Grid} from 'react-native-easy-grid';
import {
  StackActions,
  NavigationActions,
  SwitchActions,
  DrawerActions,
  type NavigationScreenProp,
} from 'react-navigation';

import {sizes, colors} from '../../css/cssVariables';
import {sansRegBlack, fonts} from '../../css/fonts';
import type {WordType} from '../../type';

import {deleteAllImages} from '../../fileSystem';

import {recentNumsOperations} from '../../ducks/recentNums';
import {themesOperations} from '../../ducks/themes';
// import {localImagesOperations} from '../../ducks/localImages';
import {favoritesOperations} from '../../ducks/favorites';
import {isIOS} from '../../utils/platform';

import {version} from '../../../package.json';

const styles = StyleSheet.create({
  logoBold: {
    fontFamily: fonts.lmLogoBold,
    fontSize: 35,
    // borderWidth: 2,
    // borderColor: 'red',
    // paddingRight: 20,
  },
  logoRegular: {
    fontFamily: fonts.lmLogoRegular,
    fontSize: 35,
  },
  sideBarStyle: {
    minHeight: Dimensions.get('window').height,
  },
  titre: {
    paddingTop: 70,
    paddingLeft: 30,
    paddingBottom: 40,
    flexGrow: 0,
    flexDirection: 'row',
    // borderWidth: 2,
    // borderColor: 'blue',
    //paddingRight: 20
  },
  bodyMenu: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    flexGrow: 10,
  },
  lineMenu: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
    fontWeight: '600',
    // borderTopWidth:1,
  },
  accordionStyle: {
    borderBottomWidth: 0,
  },
  accordionInside: {
    paddingLeft: 50,
    paddingBottom: 10,
    fontStyle: 'italic',
  },
  iconMainMenu: {
    width: 50,
    color: colors.greyDarkerLM,
    textAlign: 'center',
  },
  textMainMenu: {
    ...{
      ...sansRegBlack,
    },
  },
  textInAccordion: {
    ...{
      ...sansRegBlack,
    },
  },
  iconAccordion: {
    textAlign: 'right',
    paddingRight: 10,
    fontSize: 18,
    flexGrow: 1,
  },
});

type Props = {
  navigation: NavigationScreenProp<{}>,
  themes: Array<WordType>,
};

class Sidebar extends React.Component<Props> {
  navigateToScreen = (route, params) => {
    const {navigation} = this.props;
    const navigateAction = NavigationActions.navigate({
      routeName: route,
      params,
    });
    navigation.dispatch(navigateAction);
    if (navigation.closeDrawer) navigation.closeDrawer();
  };

  resetNavigation = () => {
    this.props.navigation.dispatch(
      SwitchActions.jumpTo({routeName: 'BottomNavigator'}),
    );
    this.props.navigation.dispatch(
      NavigationActions.navigate({routeName: 'Sommaire'}),
    );
  };

  cleanAllData = () => {
    const {cleanAll} = this.props;
    cleanAll();
  };

  hardKillImages = () => {
    deleteAllImages();
  };

  render() {
    const {themes} = this.props;

    return (
      <Container>
        <Content contentContainerStyle={styles.sideBarStyle}>
          <View style={styles.titre}>
            <Text style={styles.logoRegular}>lundi</Text>
            <Text style={styles.logoBold}>matin</Text>
          </View>

          <View style={styles.bodyMenu}>
            <View>
              <TouchableOpacity
                style={styles.lineMenu}
                onPress={this.resetNavigation}>
                <Icon style={styles.iconMainMenu} type="Entypo" name="home" />
                <Text style={styles.textMainMenu}>Sommaire</Text>
              </TouchableOpacity>
              <View>
                <Accordion
                  expanded
                  animation
                  style={styles.accordionStyle}
                  dataArray={[{title: 'Thèmes'}]}
                  renderHeader={(item, expanded) => (
                    <View style={styles.lineMenu}>
                      <Icon
                        style={styles.iconMainMenu}
                        type="FontAwesome"
                        name="newspaper-o"
                      />
                      <Text style={styles.textMainMenu}>{item.title}</Text>
                      {expanded ? (
                        <Icon
                          style={styles.iconAccordion}
                          name="remove-circle"
                        />
                      ) : (
                        <Icon
                          style={[styles.iconAccordion, {color: 'green'}]}
                          name="add-circle"
                        />
                      )}
                    </View>
                  )}
                  renderContent={() => (
                    <Content>
                      {themes.map(theme => (
                        <TouchableOpacity
                          key={theme.id}
                          style={styles.accordionInside}
                          onPress={() =>
                            this.navigateToScreen('WordSommaire', {
                              word: theme,
                            })
                          }>
                          <Text
                            style={{
                              ...styles.textMainMenu,
                              textTransform: 'capitalize',
                            }}>
                            {theme.titre}
                          </Text>
                        </TouchableOpacity>
                      ))}
                    </Content>
                  )}
                />
              </View>
              {/*------VIDÉO-------*/}
              <TouchableOpacity
                style={styles.lineMenu}
                onPress={() =>
                  this.navigateToScreen('WordSommaire', {
                    word: {
                      id: '7',
                      titre: 'Vidéos',
                      // groupe_id: '1',
                      // groupe_titre: 'Type d&#8217;article';
                    },
                  })
                }>
                <Icon style={styles.iconMainMenu} type="Entypo" name="video" />
                <Text style={styles.textMainMenu}>Vidéos</Text>
              </TouchableOpacity>
              {/*------AUDIO-------*/}
              <TouchableOpacity
                style={styles.lineMenu}
                onPress={() =>
                  this.navigateToScreen('WordSommaire', {
                    word: {
                      id: '6',
                      titre: 'Audio',
                      // groupe_id: '1',
                      // groupe_titre: 'Type d&#8217;article';
                    },
                  })
                }>
                <Icon style={styles.iconMainMenu} type="Entypo" name="radio" />
                <Text style={styles.textMainMenu}>Audio</Text>
              </TouchableOpacity>
            </View>

            <View>
              {/*------BAS DU MENU-------*/}

              {/* NOUS SUIVRE */}
              <Accordion
                expanded
                animation
                style={styles.accordionStyle}
                dataArray={[{title: 'Nous suivre'}]}
                renderHeader={(item, expanded) => (
                  <View style={styles.lineMenu}>
                    <Icon
                      style={styles.iconMainMenu}
                      type="FontAwesome"
                      name="bullhorn"
                    />
                    <Text style={styles.textMainMenu}>{item.title}</Text>
                    {expanded ? (
                      <Icon style={styles.iconAccordion} name="remove-circle" />
                    ) : (
                      <Icon
                        style={[styles.iconAccordion, {color: 'green'}]}
                        name="add-circle"
                      />
                    )}
                  </View>
                )}
                renderContent={() => (
                  <Content>
                    <TouchableOpacity
                      style={styles.accordionInside}
                      onPress={() =>
                        Linking.openURL('https://t.me/lundimatin')
                      }>
                      <Text style={styles.textMainMenu}>Telegram</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.accordionInside}
                      onPress={() =>
                        Linking.openURL('https://twitter.com/lundimat1')
                      }>
                      <Text style={styles.textMainMenu}>Twitter</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.accordionInside}
                      onPress={() =>
                        Linking.openURL('https://fr-fr.facebook.com/lundimat1/')
                      }>
                      <Text style={styles.textMainMenu}>Facebook</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.accordionInside}
                      onPress={() =>
                        Linking.openURL('https://www.instagram.com/lundi.am/')
                      }>
                      <Text style={styles.textMainMenu}>Instagram</Text>
                    </TouchableOpacity>
                  </Content>
                )}
              />

              {/* DÉVLOPPEMENT */}
              {/* <Accordion
                expanded
                animation
                style={styles.accordionStyle}
                dataArray={[{title: 'Dev'}]}
                renderHeader={(item, expanded) => (
                  <View style={styles.lineMenu}>
                    <Icon
                      style={styles.iconMainMenu}
                      type="Ionicons"
                      name="hammer"
                    />
                    <Text style={styles.textMainMenu}>{item.title}</Text>
                    {expanded ? (
                      <Icon style={styles.iconAccordion} name="remove-circle" />
                    ) : (
                      <Icon
                        style={[styles.iconAccordion, {color: 'green'}]}
                        name="add-circle"
                      />
                    )}
                  </View>
                )}
                renderContent={() => (
                  <Content>
                    <TouchableOpacity
                      style={styles.accordionInside}
                      onPress={this.cleanAllData}>
                      <Text>Ecraser toutes les données</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.accordionInside}
                      onPress={this.hardKillImages}>
                      <Text>Tuer les images à la mano</Text>
                    </TouchableOpacity>
                  </Content>
                )}
              /> */}

              {/* NOUS SOUTENIR */}
              <TouchableOpacity
                style={styles.lineMenu}
                onPress={() =>
                  Linking.openURL(
                    'https://www.helloasso.com/associations/lundimatin/formulaires/1',
                  )
                }>
                <Icon
                  style={styles.iconMainMenu}
                  type="AntDesign"
                  name="gift"
                />
                <Text style={styles.textMainMenu}>Nous Soutenir</Text>
              </TouchableOpacity>

              {/* RETOUR DE BUG */}
              <TouchableOpacity
                style={styles.lineMenu}
                onPress={() =>
                  Linking.openURL(
                    'https://app-lundimatin.typeform.com/to/kaZlCA',
                  )
                }>
                <Icon style={styles.iconMainMenu} type="Entypo" name="bug" />
                <Text style={styles.textMainMenu}>Signaler un bug</Text>
              </TouchableOpacity>

              <Text
                style={{
                  textAlign: 'center',
                  fontStyle: 'italic',
                  fontSize: 10,
                  margin: 5,
                  color: colors.greyLM,
                }}>
                Version {version} {isIOS ? 'Build 4' : null}
              </Text>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  themes: state.themes,
});
const mapDispatchToProps = dispatch => ({
  cleanAll: () => {
    dispatch(recentNumsOperations.doClearNums());
    dispatch(themesOperations.doClearThemes());
    // dispatch(localImagesOperations.doClearImages());
    dispatch(favoritesOperations.doClearFavorites());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
