// @flow

import React, {Component} from 'react';
import {
  Image,
  View,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Modal,
  Text,
} from 'react-native';
import {Icon} from 'native-base';
import HTML from 'react-native-render-html';
import {getParentsTagsRecursively} from 'react-native-render-html/src/HTMLUtils';
import {externalPath} from '../../api/constants';
import type {NavigationScreenProp} from 'react-navigation';
import openUrlInApp from '../../link';

import {
  renderNameAndBio,
  renderImage,
  renderIframe,
  renderBlockquote,
  renderNotesRef,
  renderAudio,
  renderLink,
  renderBuzzsprout,
  renderPdfDocument,
} from './renderers';
import {openLink} from '../sharedWidgets/functions';
import {fontsSizes, lineHeight, colors} from '../../css/cssVariables';
import {fonts} from '../../css/fonts';

const styles = StyleSheet.create({
  main: {
    margin: 10,
    marginRight: 20,
    color: colors.blackLM,
  },
  dot: {
    marginRight: 10,
    width: 5,
    height: 5,
    marginTop: 25,
    borderRadius: 15,
    backgroundColor: colors.blackLM,
  },
  notes: {
    paddingTop: 35,
  },
});

type Props = {
  article: Object,
  images: Array<any>,
  navigation: NavigationScreenProp<{}>,
  setIsModal: (x: boolean, y: string) => void,
  setRequiredNote: (x: string) => void,
  requiredNote?: null | string,
  setModalDownload: (x: string) => void,
};

function TextArticle({
  article,
  navigation,
  images,
  setIsModal,
  setRequiredNote,
  requiredNote,
  setModalDownload,
}: Props) {
  // hooks pour accentuer un lien quand on clique dessus
  const [isLinkTouched, setIsLinkTouched] = React.useState({
    isTouched: false,
    whichHref: '',
  });

  // la fonction pour ouvrir les liens dans l'app ou dans un browser
  function open(evt, href) {
    const route = href.replace(/.*?:\/\//g, '');
    const routeName = route.split('/')[0];
    if (routeName === 'lundi.am/') {
      openUrlInApp({url: href}, navigation);
    } else {
      openLink(evt, href);
    }
  }
  return (
    <View style={styles.main}>
      {article.texte ? (
        <HTML
          textSelectable
          html={article.texte}
          allowedStyles={[]}
          ignoredTags={[]}
          style={styles.main}
          onLinkPress={(evt, href) => {
            open(evt, href);
          }}
          tagsStyles={{
            p: {
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontFamily: fonts.textLight,
              fontSize: fontsSizes.bodyArticle,
              lineHeight: lineHeight.bodyArticle,
              paddingTop: 10,
              color: colors.blackLM,
            },
            i: {
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontFamily: fonts.textLightItalic,
              fontSize: fontsSizes.bodyArticle,
              lineHeight: lineHeight.bodyArticle,
              color: colors.blackLM,
            },
            a: {
              fontWeight: 'normal',
              fontFamily: fonts.textLightItalic,
              fontSize: fontsSizes.bodyArticle,
              lineHeight: lineHeight.bodyArticle,
              textDecorationLine: 'underline',
              color: colors.blackLM,
            },
            h2: {
              fontWeight: 'normal',
              fontFamily: fonts.textBold,
              lineHeight: 40,
              fontSize: 30,
              margin: 20,
              paddingTop: 20,
            },
            h3: {
              color: colors.redLM,
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontFamily: fonts.textBoldItalic,
              fontSize: 24,
              lineHeight: 24,
              margin: 20,
              paddingTop: 10,
            },
            dt: {
              fontWeight: 'normal',
              fontFamily: fonts.textBold,
              fontSize: fontsSizes.bodyArticle,
              lineHeight: lineHeight.bodyArticle,
              paddingTop: 15,
              paddingBottom: 10,
            },
            dl: {
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontFamily: fonts.textLight,
              fontSize: fontsSizes.bodyArticle,
              lineHeight: lineHeight.bodyArticle,
              paddingTop: 10,
            },
            li: {
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontFamily: fonts.textLight,
              fontSize: fontsSizes.bodyArticle,
              lineHeight: lineHeight.bodyArticle,
              marginTop: 15,
              color: colors.blackLM,
            },
            small: {
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontFamily: fonts.textLight,
              fontSize: fontsSizes.bodyArticle - 3,
              lineHeight: lineHeight.bodyArticle - 3,
              paddingTop: 13,
            },
            strong: {
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontFamily: fonts.textBold,
            },
          }}
          classesStyles={{
            boldItalic: {
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontFamily: fonts.textBoldItalic,
            },
            centerWrapper: {
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontFamily: fonts.textLight,
              fontSize: fontsSizes.bodyArticle,
              lineHeight: lineHeight.bodyArticle,
              paddingTop: 10,
              alignItems: 'center',
              flexGrow: 1,
              marginHorizontal: 0,
              paddingHorizontal: 0,
            },
            interviewQuestion: {
              fontWeight: 'normal',
              fontFamily: 'merriweather-bold',
              fontSize: fontsSizes.bodyArticle,
              lineHeight: lineHeight.bodyArticle,
              paddingTop: 15,
              paddingBottom: 10,
            },

            interviewReponse: {
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontFamily: 'merriweather-light',
              fontSize: fontsSizes.bodyArticle,
              lineHeight: lineHeight.bodyArticle,
              paddingTop: 10,
            },
            spip_note_ref: {
              fontFamily: fonts.textBold,
            },
            notes_text: {
              fontFamily: fonts.textBold,
            },
            spip_doc_descriptif: {
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontFamily: fonts.textLight,
              fontSize: fontsSizes.bodyArticle - 3,
              lineHeight: lineHeight.bodyArticle - 5,
              marginTop: -10,
              marginBottom: 25,
              color: colors.greyLM,
              // borderBottomWidth: 1,
              // borderBottomColor: colors.greyLM,
            },
            descriptif_italic: {
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontFamily: fonts.textLightItalic,
              fontSize: fontsSizes.bodyArticle - 3,
              lineHeight: lineHeight.bodyArticle - 5,
              paddingBottom: 20,
              color: colors.greyLM,
            },
            smallItalic: {
              fontWeight: 'normal',
              fontStyle: 'normal',
              fontFamily: fonts.textLightItalic,
              fontSize: fontsSizes.bodyArticle - 3,
              lineHeight: lineHeight.bodyArticle - 3,
              paddingBottom: 20,
              marginTop: -15,
              marginBottom: 20,
              color: colors.greyDarkerLM,
            },
          }}
          alterChildren={node => {
            const {children, name} = node;
            // pour faire en sorte que les images dans les balises <p> ne provoquent pas l'affichage en block de tous les noeuds
            // (car dans ce cas react-native les traite comme des views)
            // Si un p contient un span,
            if (
              name === 'p' &&
              children &&
              children.some(c => c.name === 'span')
            ) {
              // On recherche tous les indexs des spans qui contiennent des images

              const newNode = {
                type: 'tag',
                name: 'span',
                attribs: {class: 'anti-break-wrapper'},
              };

              const groups = [[]];

              children.forEach((child, i) => {
                const lastGroup = groups[groups.length - 1];
                if (
                  child.name &&
                  child.name === 'span' &&
                  child.attribs &&
                  child.attribs.class &&
                  child.attribs.class.includes('spip_documents')
                ) {
                  groups.push([child]);
                  groups.push([]);
                } else {
                  lastGroup.push(child);
                }
              });

              let lastFather = null;

              const newChildren = groups.map((group, i) => {
                // father est un nouveau node
                const father = Object.create(newNode);

                // on le lie aux autres
                father.parent = node;
                father.prev = lastFather;
                father.next = null;
                if (lastFather) {
                  // rétroactivement on lie le précédent à lui,
                  lastFather.next = father;
                }
                if (group.length > 0) {
                  // Son premier fils n'a plus d'aîné, son dernier plus de cadet
                  group[0].prev = null;
                  group[group.length - 1].next = null;
                }
                // On lui donne ses enfants
                father.children = group;
                lastFather = father;
                return father;
              });

              return newChildren;
            }
          }}
          alterNode={node => {
            const {name, parent, attribs} = node;
            // Si on a un pdf dont le lien se présente sous la forme d'une icône de fichier pdf
            // c'est que SPIP l'a formatté en mode doccument donc qu'il est dans des balises dl
            // avec une class 'spip_documents'
            if (
              name === 'img' &&
              parent &&
              parent.name === 'a' &&
              parent.parent &&
              parent.parent.parent &&
              parent.parent.parent.name === 'dl'
            ) {
              const isDocument = parent.parent.parent.attribs.class.split(' ');
              if (
                isDocument.includes('spip_documents') === true ||
                isDocument.includes('spip_documents_center') === true
              ) {
                node.name = 'pdfDocument';
                node.type = 'tag';
              }

              return node;
            }
            if (
              name === 'script' &&
              attribs.src &&
              attribs.src.includes('buzzsprout')
            ) {
              // Si on a un script buzzsprout, on le renvoie vers le renderer spécial conçu à cet effet
              node.name = 'buzzsprout';
              node.type = 'tag';
              return node;
            }
            // on ajoute au noeud blockquote le contenu de son html (qui vient du hack de la lib)
            if (name === 'blockquote' && attribs.class === 'twitter-tweet') {
              // console.log('alter node');
              node.attribs = {...attribs, innerHTML: node.innerHTML};
              // console.log(node);
            }
            //STYLE DES LIENS EN ITALIQUES
            if (name === 'i' && parent && parent.name === 'a') {
              // Attention, we must return the same 'node' object to change the class
              // (cause it's circular)
              node.attribs = {class: 'boldItalic'};
              return node;
            }

            // pour les italics dans les small
            if (name === 'i' && parent && parent.name === 'small') {
              node.attribs = {class: 'smallItalic'};
              return node;
            }

            if (name === 'center') {
              node.attribs = {class: 'centerWrapper'};
              return node;
            }

            // Pour les italiques dans les légendes
            if (name === 'i' && parent && parent.attribs.class) {
              const regEx = /crayon document-descriptif-+[0-9]+ spip_doc_descriptif/;
              if (parent.attribs.class.match(regEx)) {
                node.attribs = {class: 'descriptif_italic'};
                return node;
              } else {
                return node;
              }
            }

            // pour les lecteurs audio
            if (
              name === 'div' &&
              attribs.class &&
              attribs.class.includes('spip_document_audio')
            ) {
              const url =
                'https://lundi.am/' + node.children[1].children[1].attribs.src;
              node.attribs = {...node.attribs, url};
              node.name = 'documentAudio';
              return node;
            }

            // Pour les notes on crée un node.name note qui va nous permettre de faire un renderer
            if (
              name === 'span' &&
              attribs.class &&
              attribs.class === 'spip_note_ref'
            ) {
              // le premier enfant[0] est "[", le deuxième est le "a",
              // et on veut le (seul) noeud qu'il contient, et particulièrement sa data.
              const key = node.children[1].children[0].data;
              node.name = 'note';
              node.attribs = {...node.attribs, key};
            }
            // Pour les notes on retire le tag 'a' et la href sinon on ne peut pas cliquer
            if (name === 'a' && parent && parent.name === 'note') {
              node.name = 'p';
              node.attribs = {class: 'notes_text'};
            }

            // STYLE DES INTERVIEWS (BALISES DD-DT-DR)
            if (
              name === 'p' &&
              getParentsTagsRecursively(parent).indexOf('dt') !== -1
            ) {
              //Pour reparer les bizarreries de spip
              node.attribs = {class: 'interviewQuestion'};
              return node;
            }

            return false;
          }}
          listsPrefixesRenderers={{
            ul: () => <View style={styles.dot} />,
          }}
          alterData={node => {
            const {parent, data, name} = node;

            if (
              parent &&
              parent.name === 'span' &&
              parent.next &&
              parent.next.data &&
              parent.next.data === ' '
            ) {
              return `${data} `;
            }
            // INTERTITRES EN MAJUSCULES
            if (parent && parent.name === 'h3') {
              return data.toUpperCase();
            }
            return false;
          }}
          renderers={{
            pdfDocument: {
              renderer: renderPdfDocument,
            },
            buzzsprout: {
              renderer: renderBuzzsprout,
            },
            note: {
              renderer: renderNotesRef,
              renderersProps: {
                setIsModal,
                setRequiredNote,
                requiredNote,
                setIsLinkTouched,
                isLinkTouched,
              },
              wrapper: 'Text',
            },
            img: {
              renderer: renderImage,
              renderersProps: {
                navigation,
                images,
              },
            },
            iframe: renderIframe,
            blockquote: renderBlockquote,
            documentAudio: renderAudio,
            a: {
              renderer: renderLink,
              renderersProps: {
                isLinkTouched,
                setIsLinkTouched,
                setModalDownload,
                setIsModal,
              },
            },
          }}
        />
      ) : null}

      {article.notes !== '' ? (
        <View style={styles.notes}>
          <HTML
            textSelectable
            html={article.notes}
            allowedStyles={[]}
            onLinkPress={(evt, href) => {
              open(evt, href);
            }}
            tagsStyles={{
              p: {
                fontFamily: fonts.textLight,
                color: colors.blackLM,
                fontSize: fontsSizes.bodyArticle - 2,
                marginVertical: 2,
              },
              span: {
                fontFamily: fonts.textBold,
              },
              a: {
                color: colors.blackLM,
                textDecorationLine: 'none',
              },
              h2: {
                fontWeight: 'normal',
                fontFamily: fonts.textBold,
                fontSize: 25,
                marginLeft: 20,
                lineHeight: lineHeight.bodyArticle,
                paddingTop: 10,
              },
              h3: {
                color: colors.redLM,
                fontWeight: 'normal',
                fontStyle: 'normal',
                fontFamily: fonts.textBoldItalic,
                fontSize: 24,
                lineHeight: 24,
                margin: 20,
                paddingTop: 10,
              },
              dt: {
                fontWeight: 'normal',
                fontFamily: fonts.textBold,
                fontSize: fontsSizes.bodyArticle,
                lineHeight: lineHeight.bodyArticle,
                paddingTop: 15,
                paddingBottom: 10,
              },
              dl: {
                fontWeight: 'normal',
                fontStyle: 'normal',
                fontFamily: fonts.textLight,
              },
              i: {
                fontStyle: 'normal',
                fontFamily: fonts.textLightItalic,
                color: colors.blackLM,
                fontSize: fontsSizes.bodyArticle - 2,
              },
            }}
          />
        </View>
      ) : null}

      {typeof article.auteurs !== 'undefined' &&
        article.auteurs.length !== 0 &&
        article.auteurs.map(
          auteur =>
            Boolean(auteur.bio) && (
              <View key={auteur.nom}>
                <HTML
                  textSelectable
                  html={`<image>${auteur.logo}</image>`}
                  renderers={{
                    image: () => (
                      <Image
                        style={{
                          borderRadius: 50,
                          flex: 1,
                          marginRight: 'auto',
                          marginLeft: 'auto',
                          marginTop: 50,
                          marginBottom: 20,
                          height: 100,
                          width: 100,
                        }}
                        source={{uri: externalPath + auteur.logo}}
                      />
                    ),
                  }}
                />
                <View>
                  <HTML
                    textSelectable
                    html={`<div>${auteur.nom} ${auteur.bio}</div>`}
                    renderers={{
                      div: renderNameAndBio,
                    }}
                  />
                </View>
              </View>
            ),
        )}
    </View>
  );
}

export default TextArticle;
