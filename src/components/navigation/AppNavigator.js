// @flow
import React from 'react';
import {
  createAppContainer,
  NavigationScreenProp,
  NavigationState,
} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createDrawerNavigator} from 'react-navigation-drawer';

// drawerNav components
import Sidebar from './Sidebar';
import Word from '../Word';
import Settings from '../Settings';
import Support from '../Support';

// BottomNavBar component
import BottomNavBar from './BottomNavBar';
import Sommaire from '../Sommaire/SommairePrimary';
import FavoritesSommaire from '../Favorites/FavoritesSommaire';
import Search from '../Search';

// articles components
import Numero from '../Numero/Numero';
import ImageOnly from '../Article/ImageOnly';
import ArticleOnly from '../Article/ArticleOnly';
import Article from '../Article/Article';

type Props = {
  navigationState: NavigationState,
  navigation: NavigationScreenProp<>,
};

const SommaireNav = createStackNavigator(
  {
    Sommaire: {screen: Sommaire},
    Numero: {screen: Numero},
    ArticleOnly: {screen: ArticleOnly},
    ImageOnly: {screen: ImageOnly},
  },
  {
    headerMode: 'none',
  },
);

const FavoritesNav = createStackNavigator(
  {
    FavoritesSommaire: {screen: FavoritesSommaire},
    Favorite: {screen: Numero},
    ArticleOnly: {screen: ArticleOnly},
    ImageOnly: {screen: ImageOnly},
  },
  {
    headerMode: 'none',
  },
);

const SearchNav = createStackNavigator(
  {
    SearchSommaire: {screen: Search},
    SearchArticle: {screen: ArticleOnly},
    ImageOnly: {screen: ImageOnly},
  },
  {
    headerMode: 'none',
  },
);

const WordNav = createStackNavigator(
  {
    WordSommaire: {screen: Word},
    WordArticle: {screen: ArticleOnly},
    ImageOnly: {screen: ImageOnly},
  },
  {
    headerMode: 'none',
  },
);

SommaireNav.navigationOptions = ({navigation}: Props) => {
  let tabBarVisible = true;
  let swipeEnabled = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
    swipeEnabled = false;
  }
  return {
    tabBarVisible,
    swipeEnabled,
  };
};

SearchNav.navigationOptions = ({navigation}: Props) => {
  let tabBarVisible = true;
  let swipeEnabled = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
    swipeEnabled = false;
  }
  return {
    tabBarVisible,
    swipeEnabled,
  };
};

FavoritesNav.navigationOptions = ({navigation}: Props) => {
  let tabBarVisible = true;
  let swipeEnabled = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
    swipeEnabled = false;
  }
  return {
    tabBarVisible,
    swipeEnabled,
  };
};

const BottomNavigator = createMaterialTopTabNavigator(
  {
    Sommaire: {screen: SommaireNav},
    Favoris: {screen: FavoritesNav},
    Rechercher: {screen: SearchNav},
  },
  {
    tabBarPosition: 'bottom',
    tabBarComponent: ({navigationState, navigation}: Props) => (
      <BottomNavBar navigationState={navigationState} navigation={navigation} />
    ),
  },
);

const DrawerNav = createDrawerNavigator(
  {
    BottomNavigator: {screen: BottomNavigator},
    // Sommaire: {screen: SommaireNav},
    // Favoris: {screen: FavoritesNav},
    // Rechercher: {screen: SearchNav},
    Word: {screen: WordNav},
    Settings: {screen: Settings},
    Support: {screen: Support},
  },
  {
    contentComponent: Sidebar,
    drawerWidth: 301,
    drawerType: 'back',
  },
);

export default createAppContainer(DrawerNav);
