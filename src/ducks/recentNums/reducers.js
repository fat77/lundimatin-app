// @flow

import { combineReducers } from 'redux';

// importing flowTypes
import type { stateType } from '../../type';

// importing action types and flowTypes for actions
import {
  types,
  type UpdateRequest,
  type UpdateSuccess,
  type UpdateFailure,
  type ClearData,
  type RefreshNewArticles,
  type RemoveFromNewArticles,
  type ClearNewArticles,
} from './actions';

// Defining Action type
type actionType =
  | UpdateRequest
  | UpdateSuccess
  | UpdateFailure
  | ClearData
  | RefreshNewArticles
  | RemoveFromNewArticles
  | ClearNewArticles

// Defining Reducer type
type recentNumsType = $PropertyType<stateType, 'recentNums'>
type dataType = $PropertyType<recentNumsType, 'data'>
type isFetchingType = $PropertyType<recentNumsType, 'isFetching'>

// Reducer
function data(state: dataType = [], action: actionType) {
  switch (action.type) {
    case types.UPDATE_SUCCESS: {
      return action.payload.nums;
    }
    case types.CLEAR_DATA: {
      return [];
    }
    default: {
      return state;
    }
  }
}

function isFetching(state: isFetchingType = false, action: actionType) {
  switch (action.type) {
    case types.UPDATE_REQUEST: {
      return true;
    }
    case types.UPDATE_SUCCESS: {
      return false;
    }
    case types.UPDATE_FAILURE: {
      return false;
    }
    default: {
      return state;
    }
  }
}

function newArticles(state: Array<string> = [], action: actionType) {
  switch (action.type) {
    case types.REFRESH_NEW_ARTICLES: {
      return action.payload.articleIds;
    }
    case types.REMOVE_FROM_NEW_ARTICLES: {
      const { id } = action.payload;
      return state.filter(stateId => stateId !== id);
    }
    case types.CLEAR_NEW_ARTICLES: {
      const { articleIds } = action.payload;
      // return state.filter(id => !articleIds.includes(id));
      return [];
    }
    default: {
      return state;
    }
  }
}

export default combineReducers({ data, newArticles, isFetching });
