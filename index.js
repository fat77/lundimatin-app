/**
 * @format
 */
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
// import { Client } from 'bugsnag-react-native';
import App from './App';
import {name as appName} from './app.json';

// const bugsnag = new Client("0198f888e83c69e8a91da1e150c537e7");

AppRegistry.registerComponent(appName, () => App);
