// @flow

import type {NavigationScreenProp} from 'react-navigation';
import type {articleType, numeroType} from '../type';

import API from '../api/requests';

const openUrlInApp = (
  event: any,
  // recentNums: Array<numeroType>,
  // recentNumList: Array<string>,
  navigation: NavigationScreenProp<{
    params: {
      articleIndex: number,
      articles: Array<articleType>,
      swiperName: string,
    },
  }>,
) => {
  if (event.url) {
    const goTo = async url => {
      const route = url.replace(/.*?:\/\//g, '');
      // const routeName = route.split('/')[0];
      const id = route.split('lundi.am/')[1];

      // ouvre l'article
      const openArticle = newArt => {
        navigation.navigate('ArticleOnly', {
          indexNum: 0,
          article: newArt,
          swiperName: `#${newArt.numero.numero}`,
        });
      };
      // si le slug est un num ou si c'est une string
      if (id.match(/^\d+$/)) {
        const newArt = await API.getArticle(id);
        openArticle(newArt);
      } else {
        const newArt = await API.getArticleFromSlug(id);
        openArticle(newArt);
      }
    };
    goTo(event.url);
  }
};

export default openUrlInApp;
