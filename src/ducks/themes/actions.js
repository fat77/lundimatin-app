// @flow

import type { WordType } from '../../type';

const UPDATE_REQUEST = 'lm/themes/UPDATE_REQUEST';
type UpdateRequestType = 'lm/themes/UPDATE_REQUEST';
const UPDATE_SUCCESS = 'lm/themes/UPDATE_SUCCESS';
type UpdateSuccessType = 'lm/themes/UPDATE_SUCCESS';
const UPDATE_FAILURE = 'lm/themes/UPDATE_FAILURE';
type UpdateFailureType = 'lm/themes/UPDATE_FAILURE';
const CLEAR_DATA = 'lm/themes/CLEAR_DATA';
type ClearDataType = 'lm/themes/CLEAR_DATA';

export type UpdateRequest = { type: UpdateRequestType }
const updateRequest = (): UpdateRequest => ({
  type: UPDATE_REQUEST,
});

export type UpdateSuccess = { type: UpdateSuccessType, payload: {themes: Array<WordType>}}
const updateSuccess = (themes: Array<WordType>): UpdateSuccess => ({
  type: UPDATE_SUCCESS,
  payload: { themes },
});

export type UpdateFailure = { type: UpdateFailureType, payload: {error: any}}
const updateFailure = (error: any) => ({
  type: UPDATE_FAILURE,
  payload: { error },
});

export type ClearData = { type: ClearDataType }
const clearData = (): ClearData => ({
  type: CLEAR_DATA,
});

export default {
  updateRequest,
  updateSuccess,
  updateFailure,
  clearData,
};

export const types = {
  UPDATE_REQUEST,
  UPDATE_SUCCESS,
  UPDATE_FAILURE,
  CLEAR_DATA,
};
