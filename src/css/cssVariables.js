// @flow
import {Dimensions, Platform, StatusBar} from 'react-native';

const X_WIDTH = 375;
const X_HEIGHT = 812;
const XSMAX_WIDTH = 414;
const XSMAX_HEIGHT = 896;
const {height, width} = Dimensions.get('window');
const isIPhoneX = () =>
  Platform.OS === 'ios' && !Platform.isPad && !Platform.isTVOS
    ? (width === X_WIDTH && height === X_HEIGHT) ||
      (width === XSMAX_WIDTH && height === XSMAX_HEIGHT)
    : false;

export const sizes: {
  statusBarHeight: number,
  navBarHeight: number,
  fullheight: number,
  fullWidth: number,
} = {
  statusBarHeight: Platform.select({
    ios: isIPhoneX() ? 44 : 20,
    android: StatusBar.currentHeight,
    default: 0,
  }),
  navBarHeight: Platform.select({ios: 75, android: 75}),
  fullheight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width,
};

export const colors: {
  blackLM: string,
  greyLM: string,
  greyDarkerLM: string,
  whiteLM: string,
  redLM: string,
  goldLM: string,
} = {
  goldLM: 'rgba(166, 141, 93, 0.9)',
  blackLM: 'rgba(0,0,0,.84)',
  greyLM: 'rgb(150,150,150)',
  greyDarkerLM: 'rgb(80,80,80)',
  lightGreyLM: '#D2CACA',
  whiteLM: '#fdfdfd',
  redLM: 'rgb(250, 75, 42)',
};

export const fontsSizes: {
  navBar: number,
  bodyArticle: number,
} = {
  navBar: 21,
  bodyArticle: 16,
};

export const lineHeight: {
  bodyArticle: number,
} = {
  bodyArticle: 24,
};

export const scrollUp = {
  button: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    right: 20,
    bottom: 20,
    height: 30,
    width: 30,
    margin: 0,
    padding: 0,
    borderRadius: 30,
    backgroundColor: colors.whiteLM,
    opacity: 0.85,
  },
  icon: {
    margin: 0,
    padding: 0,
    fontSize: 20,
    color: colors.blackLM,
  },
};
