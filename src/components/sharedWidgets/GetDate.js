// @flow

import React from 'react';
import {Text, StyleSheet} from 'react-native';

import moment from 'moment';
import 'moment/locale/fr';
import {fonts} from '../../css/fonts';

const styles = StyleSheet.create({
  article: {
    fontFamily: fonts.sansSerifregular,
    fontSize: 14,
  },
  sommaire: {
    fontStyle: 'italic',
    color: 'rgb(110, 117, 115)',
    marginBottom: 15,
    // marginLeft: 15,
    alignSelf: 'center',
  },
});

type Props = {
  numDate: string,
  sommaireStyle: boolean,
};

const GetDate = ({numDate, sommaireStyle}: Props) => {
  const realDate = moment(numDate).format('LL');
  return (
    <Text style={sommaireStyle ? styles.sommaire : styles.article}>
      {realDate}
    </Text>
  );
};

export default GetDate;
