import React from 'react';
import AppIntroSlider from 'react-native-app-intro-slider';
import {StyleSheet, View, Image, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Icon} from 'native-base';
import {colors, sizes} from './css/cssVariables';
import {fonts} from './css/fonts';

const slides = [
  {
    key: 'bienvenu',
    title: "Bienvenu dans l'application !",
    text: 'Vous avez accès aux trois derniers numéros, même hors ligne.',
    image: require('../assets/sommairelm_square.png'),
    colors: [`${colors.whiteLM}`, 'pink'],
  },
  {
    key: 'favoris',
    title: 'Favoris',
    text: 'Enregistrez vos articles en favoris et consultez les hors ligne.',
    image: require('../assets/favoris_square.png'),
    colors: [`${colors.whiteLM}`, 'paleturquoise'],
  },
  {
    key: 'recherche',
    title: 'Recherche',
    text: 'Retrouvez tous les articles grâce au menu recherche.',
    image: require('../assets/recherche_square.png'),
    colors: [`${colors.whiteLM}`, 'peachpuff'],
  },
  {
    key: 'bug',
    title: "Évolution de l'app",
    text:
      'Vous trouvez un bug, vous avez une suggestion ? Dîtes le nous, via le formulaire de retour bug présent dans le menu de gauche',
    image: require('../assets/bug_square.png'),
    colors: [`${colors.whiteLM}`, 'pink'],
  },
];

// La taille du carré sur l'écran
const squareSize = sizes.fullWidth - 60;

const styles = StyleSheet.create({
  mainContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    // backgroundColor: colors.whiteLM,
    paddingHorizontal: 30,
    paddingBottom: 50,
  },
  image: {
    // backgroundColor: "red",
    width: squareSize,
    height: squareSize,
    borderRadius: 10
  },
  text: {
    color: colors.blackLM,
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontFamily: fonts.lmLogoRegular,
    //paddingHorizontal: 16,
    fontSize: 22,
  },
  title: {
    fontSize: 30,
    color: colors.blackLM,
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontFamily: fonts.lmLogoBold,
  },
  activeDotStyle: {
    backgroundColor: colors.goldLM,
  },
  buttonText: {
    color: colors.goldLM,
    fontSize: 30,
    fontFamily: fonts.lmLogoBold,
  },
});

const renderItem = ({item}) => {
  return (
    <LinearGradient
      style={styles.mainContent}
      useAngle={true}
      angle={0}
      angleCenter={{x: 0.5, y: 0.5}}
      colors={item.colors}
      locations={[0, 1]}>
      <Text style={styles.title}>{item.title}</Text>

        <Image style={styles.image} resizeMode="contain" source={item.image} />

      <Text style={styles.text}>{item.text}</Text>
    </LinearGradient>
  );
};

const renderNextButton = () => {
  return (
    <View>
      <Icon
        type="Feather"
        style={{fontSize: 40, color: colors.goldLM}}
        name="arrow-right"
      />
    </View>
  );
};

const renderDoneButton = () => {
  return (
    <View>
      <Icon
        type="Feather"
        style={{fontSize: 40, color: colors.goldLM}}
        name="check"
      />
    </View>
  );
};

const Introduction = ({onDone}) => {
  return (
    <AppIntroSlider
      renderItem={renderItem}
      slides={slides}
      onDone={onDone}
      activeDotStyle={styles.activeDotStyle}
      renderDoneButton={renderNextButton}
      renderNextButton={renderNextButton}
    />
  );
};

export default Introduction;
