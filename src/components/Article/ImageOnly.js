import React from 'react';
import { Modal, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Icon } from 'native-base';
import ImageViewer from 'react-native-image-zoom-viewer';

import { colors } from '../../css/cssVariables';

const ImageOnly = ({
  navigation,
  navigation: {
    state: {
      params: {
        remoteUrl,
        images,
        index,
      },
    },
  },
}) => (
    <Modal visible transparent={false} onRequestClose={() => navigation.goBack()}>
      <ImageViewer
        style={{backgroundColor: "black"}}
        imageUrls={images}
        index={index}
        loadingRender={() => {
          return (
            <ActivityIndicator
              color={colors.whiteLM}
              size="large"
              style={{
                marginRight: 'auto',
                marginLeft: 'auto',
              }}
            />

          )
        }

        }
        renderIndicator={() => null}
        saveToLocalByLongPress={false}
        enablePreload
      />
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={{
          position: 'absolute',
          top: 0,
          right: 10,
          paddingTop: 35,
          zIndex: 999,
        }}
      >
        <Icon type="AntDesign" name="closecircleo" style={{ color: colors.whiteLM }} />
      </TouchableOpacity>
    </Modal>
  );


export default ImageOnly;
