import {colors} from './cssVariables';
import {isAndroid} from '../utils/platform';

const fontsAndroid = {
  titleBold: 'alegreya-extra-bold',
  sansSerifregular: 'open-sans-regular',
  sansSerifItalic: 'open-sans-italic',
  sansSerifLight: 'open-sans-light',
  textRegular: 'merriweather-regular',
  textBold: 'merriweather-bold',
  textBoldItalic: 'merriweather-bold-italic',
  textLight: 'merriweather-light',
  textLightItalic: 'merriweather-light-italic',
  lmLogoRegular: 'pt-sans-regular',
  lmLogoBold: 'pt-sans-bold',
};

const fontIOS = {
  titleBold: 'Alegreya-ExtraBold',
  sansSerifregular: 'OpenSans-Regular',
  sansSerifItalic: 'OpenSans-Italic',
  sansSerifLight: 'OpenSans-Light',
  textRegular: 'Merriweather-Regular',
  textBold: 'Merriweather-Bold',
  textBoldItalic: 'Merriweather-BoldItalic',
  textLight: 'Merriweather-Light',
  textLightItalic: 'Merriweather-LightItalic',
  lmLogoItalic: 'PTSans-Italic',
  lmLogoBoldItalic: 'PTSans-BoldItalic',
  lmLogoRegular: 'PTSans-Regular',
  lmLogoBold: 'PTSans-Bold',
};

export const fonts = isAndroid ? fontsAndroid : fontIOS;

export const artHeader = {
  fontFamily: fonts.sansSerifItalic,
  fontStyle: 'normal',
  fontWeight: 'normal',
  color: colors.blackLM,
};

export const sansRegBlack = {
  fontFamily: fonts.sansSerifregular,
  fontSize: 14,
  color: colors.blackLM,
};

export const sansRegWhite = {
  fontFamily: fonts.sansSerifregular,
  fontSize: 14,
  color: colors.whiteLM,
};

export const sansRegGrey = {
  fontFamily: fonts.sansSerifregular,
  fontSize: 14,
  color: colors.greyLM,
};

export const artText = {
  fontWeight: 'normal',
  fontStyle: 'normal',
  fontFamily: fonts.textlight,
  fontSize: 17,
  lineHeight: 22,
};
