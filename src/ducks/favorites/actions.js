// @flow

import { articleType } from '../../type'

const ADD_FAVORITE = 'lm/favorites/ADD_FAVORITE';
type AddFavoriteType = 'lm/favorites/ADD_FAVORITE';
const REMOVE_FAVORITE = 'lm/favorites/REMOVE_FAVORITE';
type RemoveFavoriteType = 'lm/favorites/REMOVE_FAVORITE';
const CLEAR_DATA = 'lm/favorites/CLEAR_DATA';
type ClearDataType = 'lm/favorites/CLEAR_DATA';

export type AddFavorite = {type: AddFavoriteType, payload: { favorite: articleType}}
const addFavorite = (favorite: articleType): AddFavorite => ({
  type: ADD_FAVORITE,
  payload: { favorite },
});

export type RemoveFavorite = {type: RemoveFavoriteType, payload: {id: string}}
const removeFavorite = (id: string): RemoveFavorite => ({
  type: REMOVE_FAVORITE,
  payload: { id },
});

export type ClearData = {type: ClearDataType}
const clearData = (): ClearData => ({
  type: CLEAR_DATA,
});


export default {
  addFavorite,
  removeFavorite,
  clearData,
};

export const types = {
  ADD_FAVORITE,
  REMOVE_FAVORITE,
  CLEAR_DATA,
};
