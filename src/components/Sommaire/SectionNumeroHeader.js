// @flow
import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

import moment from 'moment';
import 'moment/locale/fr';

import {fonts} from '../../css/fonts';
import {colors} from '../../css/cssVariables';

import GetDate from '../sharedWidgets/GetDate';

const styles = StyleSheet.create({
  numContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    paddingTop: 10,
    paddingBottom: 15,
    

  },
  num: {
    fontSize: 50,
    fontFamily: fonts.lmLogoRegular,
    // fontStyle: 'italic',
    color: colors.greyLM,
    alignItems: 'center',
    justifyContent: 'center',
  },
  date: {
    color: colors.greyLM,
    alignSelf: 'center',
    fontFamily: fonts.lmLogoRegular,
    // marginTop: -3
  }
});

type Props = {title: string, date: string};

const SectionNumeroHeader = ({title, date}: Props) => (
  <View style={styles.numContainer}>
    <Text style={styles.num}>{title}</Text>
    <Text style={styles.date}>{moment(date).format('D MMMM')}</Text>
  </View>
);

export default SectionNumeroHeader;
