// @flow

// importing flowTypes
import type { stateType } from '../../type';

// importing action types and flowTypes for actions
import {
  types,
  type UpdateRequest,
  type UpdateSuccess,
  type UpdateFailure,
  type ClearData,
} from './actions';

// Defining Action type
type actionType =
  | UpdateRequest
  | UpdateSuccess
  | UpdateFailure
  | ClearData

// Defining Reducer type
type themesType = $PropertyType<stateType, 'themes'>


function themes(state: themesType = [], action: actionType) {
  switch (action.type) {
    case types.UPDATE_SUCCESS: {
      return [...action.payload.themes];
    }
    case types.CLEAR_DATA: {
      return [];
    }
    default: {
      return state;
    }
  }
}

export default themes;
