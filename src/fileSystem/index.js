import {FileSystem} from 'react-native-unimodules';
import {externalPath} from '../api/constants';

const localPath = FileSystem.documentDirectory;

export const IMG = `${localPath}IMG/`;

export async function checkFSDirectoryCreation() {
  FileSystem.readDirectoryAsync(FileSystem.documentDirectory).then(
    async fsDir => {
      // Si IMG existe, on regarde à l'intérieur
      if (fsDir.includes('IMG')) {
        await FileSystem.readDirectoryAsync(IMG);
        return null;
      }
      return FileSystem.makeDirectoryAsync(IMG).then(() =>
        FileSystem.readDirectoryAsync(IMG),
      );
    },
  );
}

export async function downloadImageToFileSystem(url) {
  // we keep only the last part of the url
  const name = url
    .split('/')
    .slice(-1)
    .toString();
  const localUri = IMG + name;
  const storedImage = await FileSystem.downloadAsync(
    externalPath + url,
    localUri,
  );
  return storedImage.uri;
}

export async function removeImageFromFileSystem(url) {
  const name = url
    .split('/')
    .slice(-1)
    .toString();
  const localUri = IMG + name;
  const removedImage = await FileSystem.deleteAsync(localUri);
  return removedImage;
}

export function deleteAllImages() {
  FileSystem.readDirectoryAsync(FileSystem.documentDirectory).then(
    allFileURIs => {
      allFileURIs.forEach(fileURI => {
        FileSystem.deleteAsync(FileSystem.documentDirectory + fileURI);
      });
    },
  );
}
