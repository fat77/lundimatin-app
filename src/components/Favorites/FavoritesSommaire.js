// @flow

import React, {Component} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  UIManager,
  Platform,
} from 'react-native';
import {Container, Icon, Button} from 'native-base';
import {connect} from 'react-redux';
import moment from 'moment';

import {colors} from '../../css/cssVariables';
import {sansRegWhite, artText} from '../../css/fonts';
import type {articleType} from '../../type';

import HeaderWithBurger from '../sharedWidgets/HeaderWithBurger';
import FavoritesModal from './FavoritesModal';
import {displayButton} from '../sharedWidgets/functions';
import ScrollUp from '../sharedWidgets/ScrollUp';
import MiniArticle from '../MiniArticle';

// DUCKS
import {favoritesSelectors} from '../../ducks/favorites';

const styles = StyleSheet.create({
  container: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  article: {
    margin: 10,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  title: {
    textAlign: 'center',
    fontSize: 19,
    fontWeight: 'bold',
  },
  activeTitle: {
    color: 'red',
  },
  sortFavWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.blackLM,
  },
  sortFavText: {
    ...sansRegWhite,
  },
});

type Props = {
  favorites: Array<articleType>,
  navigation: any,
};

type State = {
  isScrollTopButtonVisible: boolean,
  title: string,
  isModalOn: boolean,
  selector: string,
  displayedFavs: Array<articleType>,
};

class FavoriteSommaire extends Component<Props, State> {
  static navigationOptions = {
    title: 'Favoris',
    isScrollTopButtonVisible: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      title: 'Favoris',
      isModalOn: false,
      isScrollTopButtonVisible: false,
      selector: 'date-added',
      displayedFavs: [...props.favorites].reverse(),
    };
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.displayModal = this.displayModal.bind(this);
    this.sortFav = this.sortFav.bind(this);
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.favorites !== this.props.favorites) {
      this.sortFav(this.state.selector);
    }
  };

  displayModal = () =>
    this.setState(prevState => ({isModalOn: !prevState.isModalOn}));

  sortFav = selector => {
    const sorter = () => {
      switch (selector) {
        case 'date-added':
          return [...this.props.favorites].reverse();
        case 'date-publication':
          return [...this.props.favorites].sort(
            (a, b) =>
              Date.parse(moment(b.date_publication).format()) -
              Date.parse(moment(a.date_publication).format()),
          );
        default:
          return [...this.props.favorites].reverse();
      }
    };
    this.setState({
      displayedFavs: sorter(),
      selector: selector,
      isModalOn: false,
    });
  };

  onScrollDown = event => {
    const {isScrollTopButtonVisible} = this.state;
    const isScrollingDown = displayButton(event, isScrollTopButtonVisible);
    if (isScrollingDown !== isScrollTopButtonVisible) {
      this.setState({
        isScrollTopButtonVisible: isScrollingDown,
      });
    }
  };

  // FlowType
  scroll: any;

  render() {
    const {navigation, favorites} = this.props;
    const {title, isModalOn, isScrollTopButtonVisible} = this.state;

    return favorites && favorites.length ? (
      <Container>
        <HeaderWithBurger
          title={title}
          navigation={navigation}
          rightFunction={this.displayModal}
        />

        <ScrollView
          style={styles.container}
          ref={c => {
            this.scroll = c;
          }}
          onScroll={this.onScrollDown}>
          <FavoritesModal
            isModalOn={isModalOn}
            displayModal={this.displayModal}
            sortFav={this.sortFav}
            selector={this.state.selector}
          />
          {this.state.displayedFavs.map((article, index) => (
            <MiniArticle
              key={article.id}
              article={article}
              articleIndex={index}
              articles={this.state.displayedFavs}
              // swiperName="Favoris"
              // => Pas de swiperName : pas de titre au swiper
              navigation={navigation}
              whereToNavigate="Favorite"
            />
          ))}
        </ScrollView>
        {isScrollTopButtonVisible ? (
          <ScrollUp
            isScrollTopButtonVisible={isScrollTopButtonVisible}
            scroll={this.scroll}
          />
        ) : null}
      </Container>
    ) : (
      <Container>
        <HeaderWithBurger navigation={navigation} title={title} />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{...artText, textAlign: 'center'}}>
            Vous n'avez enregistré aucun article dans les favoris pour le
            moment. Garder des articles dans les favoris vous permet de les lire
            hors-ligne et d'y accéder plus facilement.
          </Text>
          <Text style={{...artText, textAlign: 'center'}}>
            Pour placer des articles dans cette section rien de plus facile. il
            suffit de cliquer sur l'icone des favoris placée dans chaque article
            :
          </Text>
          <Icon
            style={{paddingVertical: 10}}
            type="FontAwesome"
            name="bookmark-o"
          />
          <Text style={{...artText, textAlign: 'center'}}>
            Dès que celle-ci devient complétement noire l'article est accessible
            ici.
          </Text>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  favorites: favoritesSelectors.getFavoritesData(state),
});

export default connect(mapStateToProps)(FavoriteSommaire);
