// @flow
import type { articleType } from '../../type';
import actions from './actions';

function doAddFavorite(favorite: articleType) {
  return actions.addFavorite(favorite);
}

function doRemoveFavorite(id: string) {
  return actions.removeFavorite(id);
}

function doClearFavorites() {
  return actions.clearData();
}

const doToggleFavorite = (isFavorite: boolean, article: articleType) => {
  if (isFavorite) {
    return doRemoveFavorite(article.id);
  }
  return doAddFavorite(article);
};

export default {
  doAddFavorite,
  doRemoveFavorite,
  doClearFavorites,
  doToggleFavorite,
};
