// @flow

import React, {Component} from 'react';
import {
  RefreshControl,
  SectionList,
  TouchableOpacity,
  Linking,
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
  BackHandler,
} from 'react-native';
import {Icon, Button} from 'native-base';
import {connect} from 'react-redux';
import 'moment/locale/fr';

import type {NavigationScreenProp} from 'react-navigation';

import {scrollUp, colors, sizes} from '../../css/cssVariables';

import type {numeroType, articleType, stateType} from '../../type';

import {displayButton} from '../sharedWidgets/functions';
// ACTIONS
import {
  recentNumsSelectors,
  recentNumsOperations,
} from '../../ducks/recentNums';
import {themesOperations} from '../../ducks/themes';

import openUrlInApp from '../../link';

import MiniArticle from '../MiniArticle';
import SectionNumeroHeader from './SectionNumeroHeader';
import API from '../../api/requests';

const styles = StyleSheet.create({
  burger: {
    position: 'absolute',
    paddingTop: sizes.statusBarHeight,
    //left: 20,
    zIndex: 1000,
  },
  sectionList: {
    //paddingTop: -20,
    marginTop: sizes.statusBarHeight,
  },
  black: {
    color: colors.blackLM,
  },
  prevNumIndicator: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    padding: 10,
    borderRadius: 30,
    backgroundColor: colors.whiteLM,
    opacity: 0.85,
  },
  prevNumLoader: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    padding: 10,
    borderRadius: 30,
    backgroundColor: colors.whiteLM,
    opacity: 0.85,
  },
  nextNumIndicator: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    padding: 10,
    borderRadius: 30,
    backgroundColor: colors.whiteLM,
    opacity: 0.85,
  },
  nextNumLoader: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    padding: 10,
    borderRadius: 30,
    backgroundColor: colors.whiteLM,
    opacity: 0.85,
  },
});

type Props = {
  navigation: NavigationScreenProp<{
    params: {
      articleIndex: number,
      articles: Array<articleType>,
      swiperName: string,
    },
  }>,
  isFetching: boolean,

  recentNumList: Array<string>,
  recentNums: Array<numeroType>,
  sortedNums: Array<numeroType>,
  updateNums: () => void,
};

type State = {
  isLoading: boolean,
  veryLastNum: number,
  topNum: number,
  bottomNum: number,
  prevNumIndicator: boolean,
  nextNumIndicator: boolean,
  isScrollTopButtonVisible: boolean,
  title?: string,
  displayedNums: Array<numeroType>,
  sections: Array<any>,
};

// Fonction pour fabriquer les sections à partir des nums (utilisée à plusieurs endroits)
const getSectionsFromNums = nums =>
  nums.map(n => ({
    title: `#${n.numero}`,
    data: n.articles,
    // mapping pour retrouver l'id ou les dates sans le corps html
    mapping: n.articles.map(a => ({
      date: a.date_publication,
      id: a.id,
    })),
  }));

class Sommaire extends Component<Props, State> {
  static navigationOptions = {
    title: 'Sommaire',
  };
  list: any;
  offSet: number;
  backButtonSommaire: any;
  constructor(props) {
    super(props);
    // last num in store is last displayed and also last at all
    const currentTopNum = Math.max(
      ...props.sortedNums.map(n => parseInt(n.numero)),
    );
    this.state = {
      isScrollTopButtonVisible: false,
      displayedNums: this.props.sortedNums,
      isLoading: false,
      topNum: currentTopNum,
      bottomNum: Math.min(...props.sortedNums.map(n => parseInt(n.numero))),
      veryLastNum: currentTopNum,
      nextNumIndicator: false,
      prevNumIndicator: false,
      sections: getSectionsFromNums(this.props.sortedNums),
    };

    this.offSet = 0;
    this.list = React.createRef();
    this.onRefresh = this.onRefresh.bind(this);
  }

  componentDidMount() {
    const {recentNums, recentNumList, navigation} = this.props;
    Linking.getInitialURL()
      .then(url => {
        openUrlInApp(
          {url},
          // recentNums,
          // recentNumList,
          navigation,
        );
      })
      .catch(error => console.error(error));
    Linking.addEventListener('url', event =>
      openUrlInApp(
        event,
        // recentNums,
        // recentNumList,
        navigation,
      ),
    );
    // boutton back pour sortir de l'app
    this.backButtonSommaire = BackHandler.addEventListener(
      'hardwareBackPress',
      () => {
        if (navigation.isFocused()) {
          BackHandler.exitApp();
          return true;
        }
      },
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.sortedNums !== this.props.sortedNums) {
      const sections = getSectionsFromNums(this.props.sortedNums);

      this.setState({displayedNums: this.props.sortedNums, sections: sections});
    }
  }

  componentWillUnmount() {
    const {recentNums, recentNumList, navigation} = this.props;

    Linking.removeEventListener('url', event =>
      openUrlInApp(
        event,
        // recentNums,
        // recentNumList,
        navigation,
      ),
    );

    // on retire l'event du boutonback
    this.backButtonSommaire.remove();
  }

  onRefresh = () => {
    const {updateNums} = this.props;
    updateNums();
  };

  seePreviousNum = async () => {
    // On signale le chargement
    this.setState({isLoading: true});
    const {displayedNums} = this.state;
    // Trouver le plus ancien numéro
    let oldestNumNumber = Math.min(
      ...this.state.displayedNums.map(n => parseInt(n.numero)),
    );
    // Trouver le plus récent numéro
    let newestNumNumber = Math.max(
      ...this.state.displayedNums.map(n => parseInt(n.numero)),
    );
    // Télécharger le numéro précédent
    const olderNum = await API.getNumero((oldestNumNumber - 1).toString());
    // Retirer le plus récent numéro
    const filteredDisplayedNums = displayedNums.filter(
      n => parseInt(n.numero) !== newestNumNumber,
    );
    // Pour éviter le scroll dégueu, on y va en 3 temps :
    // D'abord enlever le numéro le plus récent
    this.setState({displayedNums: filteredDisplayedNums});
    // Puis ajouter un nouveau

    setTimeout(() => {
      this.setState(prevState => {
        const newDisplayedNums = [...prevState.displayedNums, olderNum];

        return {
          displayedNums: newDisplayedNums,
          topNum: newestNumNumber - 1,
          bottomNum: oldestNumNumber - 1,
          sections: getSectionsFromNums(newDisplayedNums),
        };
      });
      // enfin, petit scroll
      setTimeout(() => {
        this.list.scrollToLocation({
          sectionIndex: 2,
          itemIndex: 0,
          viewPosition: 0,
        });
        // this.setState({isLoadingNext: false});
      }, 500);
    }, 100);
    setTimeout(() => {
      this.setState({isLoading: false});
    }, 2000);
  };

  seeNextNum = async () => {
    // On signale le chargement
    this.setState({isLoading: true});

    const {displayedNums} = this.state;
    // Trouver le plus ancien numéro
    let oldestNumNumber = Math.min(
      ...this.state.displayedNums.map(n => parseInt(n.numero)),
    );
    // Trouver le plus récent numéro
    let newestNumNumber = Math.max(
      ...this.state.displayedNums.map(n => parseInt(n.numero)),
    );

    // Télécharger le numéro suivant
    const newerNum = await API.getNumero((newestNumNumber + 1).toString());
    // Retirer le plus ancien numéro
    const filteredDisplayedNums = displayedNums.filter(
      n => parseInt(n.numero) !== oldestNumNumber,
    );
    // Pour éviter le scroll dégueu, on y va en 3 temps :
    // D'abord enlever le numéro le plus ancien
    this.setState({displayedNums: filteredDisplayedNums});
    // Puis ajouter un nouveau
    setTimeout(() => {
      this.setState(prevState => {
        const newDisplayedNums = [newerNum, ...prevState.displayedNums];

        return {
          displayedNums: newDisplayedNums,
          topNum: newestNumNumber + 1,
          bottomNum: oldestNumNumber + 1,
          sections: getSectionsFromNums(newDisplayedNums),
        };
      });

      // enfin, petit scroll
      setTimeout(() => {
        this.list.scrollToLocation({
          sectionIndex: 0,
          itemIndex: 0,
          viewPosition: 0,
        });
        // this.setState({isLoadingNext: false});
      }, 500);
    }, 100);
    setTimeout(() => {
      this.setState({isLoading: false});
    }, 2000);
  };

  renderItem = ({item, section}) => {
    const {navigation} = this.props;

    return (
      <MiniArticle
        key={item.id}
        article={item}
        articleIndex={section.data.map(article => article.id).indexOf(item.id)}
        articles={section.data}
        mapping={section.mapping}
        swiperName={section.title}
        navigation={navigation}
        whereToNavigate="Numero"
      />
    );
  };

  scrollToSection = () => {
    setTimeout(() => {
      if (this.list) {
        this.list.scrollToLocation({
          animated: true,
          itemIndex: 0,
          sectionIndex: 0,
          viewPosition: 0.5,
        });
      }
    }, 50);
  };

  onScroll = event => {
    // Logique pour le scrollToTopButton
    const {isScrollTopButtonVisible} = this.state;
    const scroll = displayButton(event, isScrollTopButtonVisible, this.offSet);
    if (scroll.isScrollingDown !== isScrollTopButtonVisible) {
      this.setState({
        isScrollTopButtonVisible: scroll.isScrollingDown,
      });
    }

    // Logique pour faire apparaître le numéro suivant / précédent
    if (this.offSet > event.nativeEvent.contentSize.height - 1000) {
      if (this.state.prevNumIndicator === false) {
        this.setState({prevNumIndicator: true});
      }
    } else {
      if (this.state.prevNumIndicator === true) {
        this.setState({prevNumIndicator: false});
      }
    }

    if (this.state.topNum < this.state.veryLastNum && this.offSet < 500) {
      if (this.state.nextNumIndicator === false) {
        this.setState({nextNumIndicator: true});
      }
    } else {
      if (this.state.nextNumIndicator === true) {
        this.setState({nextNumIndicator: false});
      }
    }

    this.offSet = scroll.newViewOffset;
  };

  render() {
    const {navigation, isFetching, sortedNums} = this.props;
    const {isScrollTopButtonVisible, displayedNums} = this.state;
    return (
      <View style={{flex: 1}}>
        {/* MENU */}
        <View style={styles.burger}>
          <Button transparent onPress={() => navigation.openDrawer()}>
            <Icon style={styles.black} name="menu" />
          </Button>
        </View>

        {/* LIST */}
        {displayedNums.length > 0 && (
          <SectionList
            style={styles.sectionList}
            renderItem={this.renderItem}
            renderSectionHeader={({section: {title, data}}) => (
              <SectionNumeroHeader
                date={data[0].date_publication}
                title={title}
                key={title}
              />
            )}
            sections={this.state.sections}
            keyExtractor={item => item.id}
            stickySectionHeadersEnabled
            windowSize={45}
            refreshControl={
              <RefreshControl
                refreshing={isFetching}
                onRefresh={this.onRefresh}
              />
            }
            ref={ref => {
              if (ref) {
                this.list = ref;
              }
            }}
            onScroll={this.onScroll}
            scrollEventThrottle={16}
          />
        )}

        {/* SCROLL TO TOP */}
        {isScrollTopButtonVisible ? (
          <TouchableOpacity
            activeOpacity={1}
            style={scrollUp.button}
            onPress={() => {
              this.scrollToSection();
            }}>
            <Icon style={scrollUp.icon} type="AntDesign" name="upcircleo" />
          </TouchableOpacity>
        ) : null}

        {/* PREV NUM */}
        {this.state.prevNumIndicator && (
          <View
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              bottom: 0,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {!this.state.isLoading ? (
              <TouchableOpacity
                style={styles.prevNumIndicator}
                onPress={this.seePreviousNum}>
                <Text style={{fontSize: 16}}>
                  Numéro précédent{' '}
                  <Icon style={{fontSize: 14}} name="down" type="AntDesign" />
                </Text>
              </TouchableOpacity>
            ) : (
              <View style={styles.prevNumLoader}>
                <ActivityIndicator size={24} color="black" />
              </View>
            )}
          </View>
        )}

        {/* NEXT NUM */}
        {this.state.nextNumIndicator &&
          this.state.veryLastNum !== this.state.topNum && (
            <View
              style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 120,

                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {!this.state.isLoading ? (
                <TouchableOpacity
                  style={styles.nextNumIndicator}
                  onPress={this.seeNextNum}>
                  <Text style={{fontSize: 16}}>
                    Numéro suivant{' '}
                    <Icon style={{fontSize: 14}} name="up" type="AntDesign" />
                  </Text>
                </TouchableOpacity>
              ) : (
                <View style={styles.nextNumLoader}>
                  <ActivityIndicator size={24} color="black" />
                </View>
              )}
            </View>
          )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  numsInStore: state.numsInStore,
  isFetching: state.recentNums.isFetching,
  status: state.status,
  recentNums: recentNumsSelectors.getRecentNumsData((state: stateType)),
  sortedNums: recentNumsSelectors.getSortedNums((state: stateType)),
  recentNumList: recentNumsSelectors.getRecentNumList((state: stateType)),
});

const mapDispatchToProps = dispatch => ({
  updateNums: () => dispatch(recentNumsOperations.doUpdateNums()),
  updateThemes: () => dispatch(themesOperations.doUpdateThemes()),
  unNewArticle: id =>
    dispatch(recentNumsOperations.doRemoveFromNewArticles(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Sommaire);
