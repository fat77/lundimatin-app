// @flow
import React from 'react';
import {connect} from 'react-redux';
import {Image, TouchableOpacity, Dimensions} from 'react-native';

import {externalPath} from '../../api/constants';
import {IMG} from '../../fileSystem';
//import {localImagesSelectors} from '../../ducks/localImages';

const ImageSmart = ({
  url,
  // localImagesNames,
  contextImages = [],
  navigation,
}: {
  url: string,
  contextImages: Array<?{
    name: string,
    remoteBig: string,
    remoteSmall: string,
    height: string,
    width: string,
  }>,
  // localImagesNames: Array<string>,
}) => {
  // Find "radical" name for image
  const imageName = url
    .split('/')
    .slice(-1)
    .toString();

  // Find context specific to this image in article
  const imageContext = contextImages.find(image =>
    image && image.name ? image.name === imageName : false,
  );

  // Find index for slider
  const imageIndex = contextImages.findIndex(image =>
    image && image.name ? image.name === imageName : false,
  );
  // No context means no render
  if (!imageContext) {
    return null;
  }
  const {width, height, remoteBig, remoteSmall} = imageContext;
  const uri = remoteSmall;
  // Computing size based on height and width
  const maxWidth = Dimensions.get('window').width - 20;
  const [intWidth, intHeight] = [parseInt(width, 10), parseInt(height, 10)];
  const ratio = intWidth && intHeight ? intWidth / intHeight : 1;
  const newHeight = intHeight
    ? Math.min(400, intHeight, (maxWidth * 1) / ratio)
    : 400;
  const newWidth =
    intWidth && intHeight
      ? Math.min(maxWidth, Math.floor(newHeight * ratio))
      : null;

  const newStyle = {
    width: newWidth,
    height: newHeight,
  };

  return (
    <TouchableOpacity
      key={url}
      onPress={() =>
        navigation.navigate('ImageOnly', {
          remoteUrl: remoteBig,
          images: contextImages.map(image =>
            image && image.remoteBig ? {url: image.remoteBig} : {url: null},
          ),
          index: imageIndex,
        })
      }
      style={{
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Image source={{uri}} style={newStyle} />
    </TouchableOpacity>
  );
};

const mapStateToProps = state => ({
  // localImagesNames: localImagesSelectors.getLocalImagesNames(state),
  // localImages: localImagesSelectors.getLocalImages(state),
});

export default connect(mapStateToProps)(ImageSmart);
