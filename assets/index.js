const images = {
  iconHome: require('./icons8-home.svg'),
  iconBookmarks: require('./icons8-bookmark-outline.svg'),
  iconSearch: require('./icons8-search.svg'),
};

export default images;
