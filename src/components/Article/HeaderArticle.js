// @flow

import React from 'react';
import {View, SafeAreaView, TouchableOpacity, StyleSheet} from 'react-native';
import Share from 'react-native-share';
import {Icon} from 'native-base';
import {connect} from 'react-redux';
import Animated from 'react-native-reanimated';
import {isIOS} from '../../utils/platform';
// COMPONENTS
import HeaderContent from './HeaderContent';
// CSS
import {sizes, colors, fontsSizes} from '../../css/cssVariables';
import {artHeader} from '../../css/fonts';
// DUCKS
import {favoritesSelectors, favoritesOperations} from '../../ducks/favorites';
import {recentNumsSelectors} from '../../ducks/recentNums';

const styles = StyleSheet.create({
  navBar: {
    color: colors.blackLM,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    paddingTop: sizes.statusBarHeight,
    marginTop: 10,
    //paddingBottom: 10,
    backgroundColor: colors.whiteLM,
    shadowColor: 'transparent',
    // elevation: 1,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    // height: sizes.navBarHeight,
    zIndex: 2,
    alignItems: 'center',

    paddingBottom: 7,
    /* paddingBottom : 7px  => sur le pixel 3A, limite à partir de laquelle 
    on ne voit plus le second header quand la navbar se "coince" */
  },
});

type Props = {
  navigation: any,
  articleIndex: number,
  swiperName: string,
  numeroLength: number,
  scrollY: any,
  mapping: any,
};

const HeaderArticle = ({
  swiperName,
  articleIndex,
  navigation,
  numeroLength,
  scrollY,
  mapping,
}: Props) => {
  React.useEffect(() => {
    // Après chaque re-rendu, on initialise scrollY à 0
    // (à priori seul articleIndex fait re-rendre)
    scrollY.setValue(0);
  });

  const {diffClamp, interpolate, multiply} = Animated;
  // Calcul de la taille du Header à partir de scrollY
  // const scrollYFloored = scrollY.value

  const navMarginIOS = isIOS ? 40 : 0;
  const diffClampNode = diffClamp(
    scrollY,
    0,
    sizes.navBarHeight + navMarginIOS,
  );
  const animatedNavBarTranslateY = multiply(diffClampNode, -1);
  const animatedTitleOpacity = interpolate(animatedNavBarTranslateY, {
    inputRange: [-sizes.navBarHeight, 0],
    outputRange: [0, 1],
    extrapolate: 'clamp',
  });
  // Tableau abstrait (vide) de la taille du numéro, pour swiper
  return (
    // <Animated.View
    //   style={[
    //     styles.navBar,
    //     {
    //       transform: [
    //         {
    //           translateY: animatedNavBarTranslateY, // gestion du scroll à partir de scrollY
    //         },
    //       ],
    //     },
    //   ]}>
    <View style={styles.navBar}>
      <HeaderContent
        navigation={navigation}
        articleIndex={articleIndex}
        swiperName={swiperName}
        numeroLength={numeroLength}
        mapping={mapping}
        animatedTitleOpacity={animatedTitleOpacity}
      />
    </View>
    // </Animated.View>
  );
};

export default HeaderArticle;
