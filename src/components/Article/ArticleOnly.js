// @flow
import * as React from 'react';
import {
  View,
  StyleSheet,
  Platform,
  UIManager,
  TouchableOpacity,
  Text,
  Modal,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import {Icon} from 'native-base';
import {connect} from 'react-redux';
import Animated from 'react-native-reanimated';

import type {NavigationScreenProp} from 'react-navigation';
import HTML from 'react-native-render-html';
import {openLink} from '../sharedWidgets/functions';
import openUrlInApp from '../../link';
import FooterArticle from './FooterArticle';

// CSS
import {fontsSizes, scrollUp, colors, sizes} from '../../css/cssVariables';
import {fonts} from '../../css/fonts';

import {favoritesSelectors} from '../../ducks/favorites';
import type {articleType} from '../../type';
import {displayButton} from '../sharedWidgets/functions';
import {externalPath} from '../../api/constants';

// COMPONENTS
import HeaderArticleOnly from './HeaderArticleOnly';
import LogoAndTitle from './LogoAndTitle';
import SoustitreAndChapo from './SoustitreAndChapo';
import TextArticle from './TextArticle';
import Recommandations from './Recommandations';
import Skeleton from './Skeleton';
// import ScrollUp from '../sharedWidgets/ScrollUp';
import BottomModal from './BottomModal';

const {event, Value} = Animated;

const styles = StyleSheet.create({
  scrollViewContent: {
    paddingTop: 60,
    backgroundColor: colors.whiteLM,
  },
});

type Props = {
  navigation: NavigationScreenProp<>,
  article: articleType,
  indexNum: number,
  swiperName: string,
};

type State = {
  loaded: boolean,
  isScrollTopButtonVisible: boolean,
  isModal: {showModal: boolean, modalFrom: string},
  requiredNote: null | string,
  modalDownload: boolean,
  downloadText: string,
};

class ArticleOnly extends React.Component<Props, State> {
  static navigationOptions = {
    title: 'Article',
  };

  constructor() {
    super();
    this.offSet = 0;
    this.scrollY = new Value(0);
    this.state = {
      loaded: true,
      isScrollTopButtonVisible: false,
      isModal: {showModal: false, modalFrom: ''},
      requiredNote: null,
      modalDownload: false,
      downloadText: '',
    };
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  componentDidMount() {
    //this.scroll.scrollTo({x: 0, y: 0, animated: true});
    // ça servait pour le skeleton
    // setTimeout(() => {
    //   this.setState({loaded: true});
    // }, 500);
  }

  setModalDownload = (text: string) => {
    this.setState({downloadText: text});
  };

  setIsModal = (bool: boolean, from: string) => {
    const newModal = {showModal: bool, modalFrom: from};
    this.setState({isModal: newModal});
  };

  setRequiredNote = (id: string) => {
    this.setState({requiredNote: id});
  };

  onScrollDown = e => {
    const {isScrollTopButtonVisible} = this.state;
    const scroll = displayButton(e, isScrollTopButtonVisible, this.offSet);
    if (scroll.isScrollingDown !== isScrollTopButtonVisible) {
      this.setState({
        isScrollTopButtonVisible: scroll.isScrollingDown,
      });
    }
    this.offSet = scroll.newViewOffset;
  };

  goToTop = scroll => {
    scroll.getNode().scrollTo({x: 0, y: 0, animated: true});
    this.setState({
      isScrollTopButtonVisible: false,
    });
  };

  // liens vers l'app ou vers le dehors
  open(evt, href) {
    const {navigation} = this.props;
    const route = href.replace(/.*?:\/\//g, '');
    const routeName = route.split('/')[0];
    if (routeName === 'lundi.am/') {
      openUrlInApp({url: href}, navigation);
    } else {
      openLink(evt, href);
    }
  }

  // FlowType
  scroll: any;

  scrollY: number;

  offSet: number;

  render() {
    const {
      navigation,
      navigation: {
        state: {
          params: {indexNum, article, swiperName},
        },
      },
    } = this.props;

    // scroll to top

    const {
      requiredNote,
      modalDownload,
      downloadText,
      isModal,
      loaded,
      isScrollTopButtonVisible,
    } = this.state;

    const images = [
      {
        // logo
        name: article.logo_petit
          .split('/')
          .slice(-1)
          .toString(),
        remoteBig: externalPath + article.logo,
        remoteSmall: externalPath + article.logo_petit,
        height: article.logo_petit_hauteur,
        width: article.logo_petit_largeur,
      },
      ...article.images.map(
        // all images
        image => ({
          name: image.url_petit
            .split('/')
            .slice(-1)
            .toString(),
          remoteBig: externalPath + image.url,
          remoteSmall: externalPath + image.url_petit,
          width: image.largeur,
          height: image.hauteur,
        }),
      ),
    ];

    return (
      <SafeAreaView>
        <HeaderArticleOnly
          swiperName={swiperName ? swiperName : ''}
          article={article}
          indexNum={indexNum}
          scrollY={this.scrollY}
          navigation={navigation}
        />
        <Animated.ScrollView
          style={styles.scrollViewContent}
          scrollEventThrottle={1000}
          ref={c => {
            this.scroll = c;
          }}
          onScroll={event([{nativeEvent: {contentOffset: {y: this.scrollY}}}], {
            useNativeDriver: true,
          })}
          onMomentumScrollEnd={this.onScrollDown}>
          <LogoAndTitle
            fromArticleOnly={true}
            article={article}
            images={images}
            navigation={navigation}
          />
          <SoustitreAndChapo
            navigation={navigation}
            article={article}
            setIsModal={this.setIsModal}
            setRequiredNote={this.setRequiredNote}
            setModalDownload={this.setModalDownload}
          />
          {loaded ? (
            <View>
              <TextArticle
                article={article}
                images={images}
                navigation={navigation}
                setIsModal={this.setIsModal}
                setRequiredNote={this.setRequiredNote}
                requiredNote={this.state.requiredNote}
                setModalDownload={this.setModalDownload}
              />
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 50,
                }}>
                <Text
                  style={{
                    fontSize: 18,
                    fontFamily: fonts.sansSerifregular,
                  }}>
                  Articles recommandés
                </Text>
              </View>
              <Recommandations article={article} navigation={navigation} />
            </View>
          ) : (
            <View style={{margin: 10}}>
              <Skeleton numberFromProps={60} />
            </View>
          )}
          <View style={{height: sizes.navBarHeight}} />
        </Animated.ScrollView>

        {/* MODAL TO NOTIFY WHEN A FILE IS DOWNLOADED */}
        {/* AND */}
        {/* MODAL POUR LES NOTES */}

        <BottomModal
          isModal={isModal}
          setIsModal={this.setIsModal}
          modalDownload={modalDownload}
          downloadText={downloadText}
          requiredNote={requiredNote}
          article={article}
          navigation={navigation}
        />
        {isScrollTopButtonVisible ? (
          <TouchableOpacity
            activeOpacity={1}
            style={scrollUp.button}
            onPress={() => this.goToTop(this.scroll)}>
            <Icon style={scrollUp.icon} type="AntDesign" name="upcircleo" />
          </TouchableOpacity>
        ) : null}
        <FooterArticle article={article} scrollY={this.scrollY} />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  favorites: state.favorites.data,
  favoriteList: favoritesSelectors.getFavoriteList(state),
});

export default connect(mapStateToProps)(ArticleOnly);
